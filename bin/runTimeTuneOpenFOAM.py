#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import socket
from os import path
import argparse
import shlex
import optuna
import time
from collections import OrderedDict
import json
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
import re
import pathlib
import os


def parseOptions():
    """
    Parse options
    """
    p = argparse.ArgumentParser(description='Run-time bayesian optimization of solver settings for OpenFOAM solver.')

    p.add_argument('logFilename', help='log filename of solver')
    p.add_argument('--debug', type=int, default=0)
    p.add_argument('--interval', help='interval', type=int, default=2)
    p.add_argument('--redoUpdate', help='interval', type=int, default=100)
    p.add_argument('--nTrials', help='nTrials', type=int, default=1000)
    p.add_argument('--useStorage', help='useStorage', action='store_true')
    p.add_argument('--studyName', help='studyName', type=str, default='runTimeTuneOpenFOAM')
    p.add_argument('--storage', help='storage', type=str, default='sqlite:///runTimeTuneOpenFOAM.db')
    p.add_argument('--seed',type=int, default=13)
    p.add_argument('--jsonFilename', help='jsonFilename', type=str, default='runTimeTuneOpenFOAM.json')
    p.add_argument('--useInitial', action='store_true')

    return p.parse_args()


def parseConfig(filename):
    with open(filename) as f:
        df = json.load(f, object_pairs_hook=OrderedDict)

    return(df)


def follow():
    while True:
        line = logfile.readline()
        if not line:
            time.sleep(0.1)
            continue
        yield line


def executionTimePerStep(config, paramFileList):
    reRead=[ False ] * len(config)
    n=0
    t=[]
    logfile.seek(0,2)
    for i, filename in enumerate(config):
        paramFileList[i].writeFile()
        pathlib.Path(filename).touch(exist_ok=True)
        if args.debug>=1:
            print("# Update {}".format(filename), file=sys.stderr)
    for line in follow():
        if args.debug>=2:
            print(line.rstrip(), file=sys.stderr)
        if args.debug>=3:
            print("# n={}, t={}".format(n, t), file=sys.stderr)
        if all(reRead):
            if line.startswith('ExecutionTime '):
                t.append(float(line.split(' ')[2]))
                n=n+1
                if (n>args.interval):
                    break
        else:
            if line.startswith('Time = '):
                for filename in config:
                    pathlib.Path(filename).touch(exist_ok=True)
                    if args.debug>=2:
                        print("# Update time-stamp of {}".format(filename), file=sys.stderr)
                continue    

            for i, filename in enumerate(config):
                basename=os.path.basename(filename)
                str='        Re-reading object {} '.format(basename)
                if line.startswith(str):
                    reRead[i]=True
                    if args.debug>=1:
                        print("# Update confirmed {}".format(filename), file=sys.stderr)

    tave = (t[args.interval]-t[0])/float(args.interval)
    if args.debug>=1:
        print("# tave={}".format(tave), file=sys.stderr)
    return(tave)


def suggest(subConfig, trial, paramFile):
    for keys in subConfig:
#        valI = trial.suggest_categorical(keys, range(len(subConfig[keys])))
        valI = trial.suggest_int(keys, 0, len(subConfig[keys])-1)
        val=subConfig[keys][valI]
        if isinstance(val, dict):
            value=list(val.keys())[0]
        else:
            value=val

        if args.debug>=1:
            print("keys:{} value={}".format(keys, value), file=sys.stderr)

        keysList = keys.split('/')
        paramFileTmp=paramFile
        n=len(keysList)
        for keyI in range(n):
            key=keysList[keyI]
            if keyI==(n-1):
                if isinstance(val, dict):
                    if value=="":
                        paramFileTmp[key]={}
                    else:
                        paramFileTmp[key]=value
                    subsubConfig=list(val.values())[0]
                    suggest(subsubConfig, trial, paramFile)
                else:
                    paramFileTmp[key]=value
            else:
                paramFileTmp=paramFileTmp[key]

    return


def objective(trial):
    """
    Generate objective function
    """
    if hasattr(trial, 'number'):
        print("\ntrial.number={}".format(trial.number), file=sys.stderr)

    jsonFilename="{}.{}".format(args.jsonFilename,trial.number)
    if not args.useInitial or not path.exists(jsonFilename):
        jsonFilename=args.jsonFilename

    print("JSON flename={}".format(jsonFilename), file=sys.stderr)

    config = parseConfig(jsonFilename)

    paramFileList=[]
    for filename in config:
        paramFile=ParsedParameterFile(filename)
        suggest(config[filename], trial, paramFile)
        paramFileList.append(paramFile)

    tave=executionTimePerStep(config, paramFileList)
        
    return tave


def main():
    print("Hostname: {}".format(socket.gethostname()), file=sys.stderr)

    global args
    args = parseOptions()

    if args.useStorage:
        storage = args.storage
    else:
        storage = None

    study = optuna.create_study(
        study_name = args.studyName,
        storage = storage,
        sampler = optuna.samplers.TPESampler(seed=args.seed),
        load_if_exists = True
        )

    while True:
        if os.path.isfile(args.logFilename):
            break
        time.sleep(0.1)

    global logfile
    logfile = open(args.logFilename,"r")

    study.optimize(objective, n_trials = args.nTrials, n_jobs = 1)

    pruned_trials = [t for t in study.trials if t.state == optuna.structs.TrialState.PRUNED]
    timeout_trials = [t for t in study.trials if t.state == optuna.structs.TrialState.FAIL]
    complete_trials = [t for t in study.trials if t.state == optuna.structs.TrialState.COMPLETE]
    print('Study statistics: ')
    print('  Number of finished trials: ', len(study.trials))
    print('  Number of pruned trials: ', len(pruned_trials))
    print('  Number of timeout trials: ', len(timeout_trials))
    print('  Number of complete trials: ', len(complete_trials))
    trial = study.best_trial
    print('Best trial value: {:g}'.format(trial.value))


if __name__ == '__main__':
    main()
