#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import hashlib
import itertools
import json
import numpy
import os
import optuna
import pathlib
import socket
import shutil
import subprocess
import sys
import time
from collections import OrderedDict
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile

    
def parseOptions():
    """
    Parse options
    """
    p = argparse.ArgumentParser()

    p.add_argument('--debug', type=int, default=0)
    p.add_argument('--study', type=str, default='OFBT')
    p.add_argument('--useStorage', action='store_true')
    p.add_argument('--storage', type=str, default='sqlite:///OFBT.db')
    p.add_argument('--json', type=str, default='OFBT.json')
    p.add_argument('--baseDir', type=str, default='cases')


    return p.parse_args()


def parseConfig(filename):
    """
    Read JSON format configuration file
    """
    with open(filename) as f:
        df = json.load(f, object_pairs_hook=OrderedDict)

    return(df)


def submitJob(trial):
    """
    Submit job
    """
    sortedParames = {key: trial.params[key]
                     for key in sorted(trial.params.keys())}
    paramsStr = "{}".format(sortedParames)
    case = "_"+hashlib.sha1(paramsStr.encode('utf-8')).hexdigest()
    casePath = pathlib.Path(args.baseDir) / case
    casePath.mkdir(exist_ok=True)
    with open(casePath / 'BOOF.params', 'w') as f:
        f.write(paramsStr)

    # copy cases
    command = ["../../BOOF.copy"]
    print('Execute job {} at {}'.format(
        ' '.join(command), casePath), file=sys.stderr)
    jobid = 0
    try:
        command_output = subprocess.check_output(
            command,
            cwd=casePath,
            universal_newlines=True
        )
    except subprocess.CalledProcessError as e:
        print('Error in job {} at {}'.format(
            ' '.join(command), casePath), file=sys.stderr)

    paramFileTmp = None
    filenameOld = None
    for filenameKeys, value in trial.params.items():
        filename, keys = filenameKeys.split(':')
        keysList = keys.split('/')
        if filename != filenameOld:
            if filenameOld != None:
                paramFile.writeFile()
            paramFile = ParsedParameterFile(casePath / filename)
            filenameOld = filename
        paramFileTmp = paramFile
        n = len(keysList)
        for keyI in range(n):
            key = keysList[keyI]
            if keyI == (n-1):
                paramFileTmp[key] = value
            else:
                paramFileTmp = paramFileTmp[key]
    paramFile.writeFile()

    # run solver
    command = ["../../BOOF.run"]
    print('Execute job {} at {}'.format(
        ' '.join(command), casePath), file=sys.stderr)
    jobid = 0
    try:
        command_output = subprocess.check_output(
            command,
            cwd=casePath,
            universal_newlines=True
        )
        f = float(command_output.strip())
    except subprocess.CalledProcessError as e:
        print('Error in job {} at {}'.format(
            ' '.join(command), casePath), file=sys.stderr)
        f = numpy.nan

    return f


def suggest(subConfig, trial):
    """
    Suggest parameters
    """
    for keys in subConfig:
        val = trial.suggest_categorical(keys, subConfig[keys])
        if isinstance(val, dict):
            value = list(val.keys())[0]
        else:
            value = val

        if args.debug >= 1:
            print("keys={} value={}".format(keys, value), file=sys.stderr)

        if isinstance(val, dict):
            subsubConfig = list(val.values())[0]
            suggest(subsubConfig, trial)

    return


def objective(trial):
    """
    Generate objective function
    """
    if hasattr(trial, 'number'):
        print("\ntrial.number={}".format(trial.number), file=sys.stderr)

    # Generate trial object
    suggest(config, trial)

    return submitJob(trial)


def gridSearch(study):
    """
    Enque all combination of parameters for grid search
    """
    print(config)
#    paramAll={}
    for i, batch in enumerate(config):
        if i==0:
            paramAll=itertools.product(*config[batch].values())
#        else:
#            paramAll=itertools.product(paramAll, *config[batch].values())

    n = 1
    for paramList in paramAll:
        print(paramList)
        param = {key: paramList[i] for i, key in enumerate(config.keys())}
        if args.debug >= 1:
            print("param={}".format(param), file=sys.stderr)
        n = n+1

    return n

def main():
    """
    Main
    """
    print("Hostname: {}".format(socket.gethostname()), file=sys.stderr)
    print("PID: {}".format(os.getpid()), file=sys.stderr)

    global args
    args = parseOptions()

    global config
    config = parseConfig(args.json)

    # Create study
    if args.useStorage:
        storage = args.storage
    else:
        storage = None

    study = optuna.create_study(
        study_name = args.study,
        storage=storage,
        load_if_exists = True
        )

    # Grid search
    args.nTrials = gridSearch(study)
    exit(0)

    # Generate base case diretory
    pathlib.Path(args.baseDir).mkdir(exist_ok=True)

    # Optimize
    study.optimize(objective, n_trials=args.nTrials)

    # Study statistics
    pruned_trials = [t for t in study.trials if t.state ==
                     optuna.trial.TrialState.PRUNED]
    timeout_trials = [t for t in study.trials if t.state ==
                      optuna.trial.TrialState.FAIL]
    complete_trials = [t for t in study.trials if t.state ==
                       optuna.trial.TrialState.COMPLETE]

    print('\nStudy statistics: ', file=sys.stderr)
    print('  Number of finished trials: ', len(study.trials), file=sys.stderr)
    print('  Number of pruned trials: ', len(pruned_trials), file=sys.stderr)
    print('  Number of timeout  trials: ',
          len(timeout_trials), file=sys.stderr)
    print('  Number of complete trials: ', len(
        complete_trials), file=sys.stderr)


if __name__ == '__main__':
    main()
