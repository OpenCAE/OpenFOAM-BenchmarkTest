#!/bin/bash
#for problem in S M XL
for problem in S
do
    echo $problem
    (cd $problem
     ../bin/averageExecutionTime.sh cases/proc_*/cases/*/log.icoFoam.* | sort -t ',' -k 3,4 -n > time.csv
     gnuplot plot.gp
    )
done
