#!/bin/bash
awk '/^ExecutionTime/ {if (n==0) {t0=$3;print 0,0,0} else {print n,$3,($3-t0)/n};n++}' log.icoFoam.* > plotAverageExecutiontimePerTimeSteps.txt
GNUPLOT=$(which gnuplot)
[ ! "$GNUPLOT" = "" ] || die "$0 requires Gnuplot installed"
GPFILE=$(mktemp)
cat > "$GPFILE" <<EOF
set terminal pdf
set output 'plotAverageExecutiontimePerTimeSteps.pdf'
set xlabel 'Time steps [-]'
set ylabel 'Average execution time per time steps [s]'
set grid
unset key
plot 'plotAverageExecutiontimePerTimeSteps.txt' using 1:3 with l
EOF
$GNUPLOT "$GPFILE"
