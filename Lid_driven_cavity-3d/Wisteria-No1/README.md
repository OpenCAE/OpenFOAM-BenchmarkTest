# Wisteria-No1

## General information

* Measurer: IMANO Masashi
* Date: Oct 2023

## Hardware

* Name: Wisteria/BDEC-01 (https://www.cc.u-tokyo.ac.jp/en/supercomputer/)

## How to run

### Make case directories

```bash
./Allrun.pre
```

### Gerarate meshes

```bash
./Allrun.blockMesh
```

### Decompose regions

```bash
./Allrun.decomposePar
```

### Submit jobs

```bash
./Allrun.solve.CASENAME
```

* CASENAME
  * A64FX
  * Aquarius
  * RapidCFD
  * Xeon

## References
* https://gitlab.com/masaz/OpenCAEWorkshop/-/blob/master/OpenCAEWorkshopAtToyama20231014/doc/OpenCAEWorkshopAtToyama20231014.pdf (in Japanese)
