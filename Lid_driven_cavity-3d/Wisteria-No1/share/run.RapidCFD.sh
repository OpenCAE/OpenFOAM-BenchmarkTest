#!/bin/sh
module load gcc/8.3.1 cuda/11.4 ompi-cuda/4.1.1-11.4
export HOME=/work/$(id -gn)/$(id -un)
source $HOME/RapidCFD/RapidCFD-dev/etc/bashrc
env
application=icoFoam
jobid=${PJM_SUBJOBID:-$PJM_JOBID}
log=log.$application.$jobid

deviceArray=("0" "1" "2" "3" "4" "5" "6" "7")
devices="("
i=0
startdevice=0
deviceI=$startdevice
while [ "$i" -lt "$PJM_MPI_PROC" ]
do
    devices="$devices ${deviceArray[$deviceI]}"
    i=$(expr $i + 1)
    deviceI=$(expr $deviceI + 1)
    if [ "$deviceI" -eq 8 ];then
	deviceI=$startdevice
    fi
done
devices="$devices )"

if [ $PJM_MPI_PROC -eq 1 ]
then
    $application &> $log
else
    if [ $PJM_MPI_PROC -lt 8 ]
    then
	npernode=$PJM_MPI_PROC
    else
	npernode=8
    fi
    mpiexec --report-bindings --display-map -display-devel-map -machinefile $PJM_O_NODEINF -n $PJM_MPI_PROC -npernode $npernode $application -devices "$devices" -parallel &> $log
fi
