#!/bin/sh
module load cmake fftw petsc scotch
source /work/$(id -gn)/$(id -un)/OpenFOAM/OpenFOAM-v2306/etc/bashrc WM_COMPILER=FClang WM_MPLIB=FJMPI
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions
application=$(getApplication)
jobid=${PJM_SUBJOBID:-$PJM_JOBID}
log=log.$application.$jobid
env
mpiexec -of $log $application -parallel
