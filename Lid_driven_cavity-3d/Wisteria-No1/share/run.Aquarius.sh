#!/bin/sh
ppn=8
module load gcc/8.3.1 cuda/11.4 ompi-cuda/4.1.1-11.4
source /work/$(id -gn)/$(id -un)/OpenFOAM/OpenFOAM-v2306/etc/bashrc WM_COMPILER=Gcc WM_MPLIB=SYSTEMOPENMPI
eval $(foamEtcFile -sh -config petsc -- -force)
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions
env
application=$(getApplication)
jobid=${PJM_SUBJOBID:-$PJM_JOBID}
log=log.$application.$jobid
if [ $PJM_MPI_PROC -eq 1 ]
then
    $application -lib petscFoam &> $log
else
    if [ $PJM_MPI_PROC -lt $ppn ]
    then
	npernode=$PJM_MPI_PROC
    else
	npernode=$ppn
    fi
    mpiexec --report-bindings --display-map -display-devel-map -machinefile $PJM_O_NODEINF -n $PJM_MPI_PROC -npernode $npernode ../../../../../share/wrapper.sh $application -lib petscFoam -parallel &> $log
fi
