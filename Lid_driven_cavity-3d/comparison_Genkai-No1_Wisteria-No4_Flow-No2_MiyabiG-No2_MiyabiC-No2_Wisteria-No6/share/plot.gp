set terminal pdfcairo solid color lw 2 font "Helvetica,13"
set style data linespoints
set logscale x
set logscale y
set format y "10^{%T}"
set grid
set pointsize 0.75
set xlabel "Number of nodes"
set xrange [xmin:xmax]
set xtics (1./8.,1./4.,1./2.,1,2,3,4,6,8,12,16,24,32,48,64,96,128,192,384) font "Helvetica,11"
set key above font "Courier,9" Left maxcols 3 samplen 4 box linewidth 0.5

set output "node-cost.pdf"
set yrange [*:*]
set ylabel "Cost per step [yen] (<Lower is better)" 
plot \
 "Odyssey/time/v2406-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using 2:($3/3600.*$2*90000./8640.) title "A64FX,PETSc-AMG" with lp\
,"< awk '{if ($2<=32) {print $0}}' MiyabiC/time/Icx-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using 2:($3/3600.*$2*300000./8640.*0.8) title "Max9480,PETSc-AMG" with lp\
,"< sort -n Genkai/time/*cpuGccN-*-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using ($1/120):($3/3600.*$1/120.*30.) title "8490H,PETSc-AMG" with lp\
,"< sort -n Flow/time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/40):($3*$1/40.*4.*0.007/0.65) title " V100,AmgX-AMG" with lp\
,"< sort -n Wisteria/time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600.*$1/72.*90000.*3.*8./8640.) title " A100,AmgX-AMG" with lp\
,"< sort -n Genkai/time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/120):($3/3600.*$1/120.*120.) title " H100,AmgX-AMG" with lp\
,"< (awk '{if ($1>=72) {print $0}}' MiyabiG/time/GccModule-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=18) {print $0}}' MiyabiG/time/migGccModule-AmgX-PCG_CLASSICAL_V_JACOBI) | sort -n" using ($1/72):($3/3600.*$1/72.*300000./8640.) title " GH200,AmgX-AMG" with lp\

set label "Linear" at 2*xmin,0.65*timemax rotate by 0 font "Courier Bold,12"
set output "node-time.pdf"
set yrange [*:timemax]
set ylabel "Exectution time per step [h] (<Lower is better)" 
plot \
 "Odyssey/time/v2406-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using 2:($3/3600) title "A64FX,PETSc-AMG" with lp\
,"< awk '{if ($2<=32) {print $0}}' MiyabiC/time/Icx-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using ($1/112):($3/3600) title "Max9480,PETSc-AMG" with lp\
,"< sort -n Genkai/time/*cpuGccN-*-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using ($1/120):($3/3600) title "8490H,PETSc-AMG" with lp\
,"< sort -n Flow/time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/40):($3/3600) title " V100,AmgX-AMG" with lp\
,"< sort -n Wisteria/time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600) title " A100,AmgX-AMG" with lp\
,"< sort -n Genkai/time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/120):($3/3600) title " H100,AmgX-AMG" with lp\
,"< (awk '{if ($1>=72) {print $0}}' MiyabiG/time/GccModule-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=18) {print $0}}' MiyabiG/time/migGccModule-AmgX-PCG_CLASSICAL_V_JACOBI) | sort -n" using ($1/72):($3/3600) title " GH200,AmgX-AMG" with lp\
,timemax/(x/xmin) title "" with l lt 1 lc 0 lw 0.5
