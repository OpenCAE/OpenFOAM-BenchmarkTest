# comparison_Genkai-No1_Wisteria-No4_Flow-No2_MiyabiG-No2_MiyabiC-No2-Wisteria-No6

## System and software

* A64FX
  * Measurement date: Feb 2025
  * System: Wisteria/BDEC-01 Odyssey (https://www.cc.u-tokyo.ac.jp/en/supercomputer/)
  * OpenFOAM: v2406
  * Compiler: Fujitsu Compiler 1.2.39 (fj/1.2.39 module)
    * Optimize options: -O3
  * MPI: Fujitsu MPI 1.2.39 (fjmpi/1.2.39 module)
* Max9480
  * Measurement date: Jan 2025 - Feb 2025
  * System: Miyabi-C (https://www.cc.u-tokyo.ac.jp/en/supercomputer/miyabi/system.php)
  * OpenFOAM: v2406
  * Compiler: Intel oneAPI 2023.2.0 (intel/2023.2.0 module)
    * Optimize options: -O3
  * MPI: Intel MPI 2021.10.0(impi/2021.10.0 module)
* 8490H
  * Measurement date: Jul 2024 - Sep 2024
  * System: Genkai-A (https://www.cc.kyushu-u.ac.jp/scp/system/Genkai/)
  * OpenFOAM: v2406
  * Compiler: Gcc 12 (gcc-toolset/12 module)
    * Optimize options: -O3
  * MPI: OpenMPI 4.1.6(ompi/4.1.6 module)
  * AmgX: Git master branch
  * PETSc: 3.22.2
    * Optimize options: -O3
* H100
  * Measurement date: Jul 2024 - Sep 2024
  * System: Genkai-B (https://www.cc.kyushu-u.ac.jp/scp/system/Genkai/)
  * OpenFOAM: v2406
  * Compiler: Gcc 12 (gcc-toolset/12 module)
    * Optimize options: -O3
  * CUDA: CUDA 12.2.2 (cuda/12.2.2 module)
  * MPI: OpenMPI 4.1.6(ompi/4.1.6-12.2.2 module)
  * AmgX: Git master branch
  * PETSc: 3.22.2
    * Optimize options: -O3
* A100
  * Measurement date: Jul 2024
  * System: Wisteria/BDEC-01 Aquarius (https://www.cc.u-tokyo.ac.jp/en/supercomputer/wisteria/system.php)
  * OpenFOAM: v2406
  * Compiler: Gcc 12.2.0 (gcc/12.2.0 module)
    * Optimize options: -O3
  * CUDA: CUDA 12.0 (cuda/12.0 module)
  * MPI: OpenMPI 4.1.6(ompi/4.1.6-12.0 module)
  * AmgX: Git master branch
  * PETSc: 3.22.2
    * Optimize options: -O3
* V100
  * Measurement date: Jul 2024
  * System: "Flow" Type II (https://icts.nagoya-u.ac.jp/ja/sc/overview.html)
  * OpenFOAM: v2406
  * Compiler: Gcc 11.3.0 (gcc/11.3.0 module)
    * Optimize options: -O3
  * CUDA: CUDA 12.1.1 (cuda/12.1.1 module)
  * MPI: OpenMPI 4.0.5(ompi/4.0.5 module)
  * AmgX: Git master branch
  * PETSc: 3.22.2
    * Optimize options: -O3
* GH200
  * Measurement date: Jan 2025 - Feb 2025
  * System: Miyabi-G (https://www.cc.u-tokyo.ac.jp/en/supercomputer/miyabi/system.php)
  * OpenFOAM: v2406
  * Compiler: GCC 12.4.0 (gcc/12.4.0 module)
    * Optimize options: -O3
  * CUDA: 12.6 (cuda/12.6 module)
  * MPI: OpenMPI 4.1.6(ompi/4.1.6-12.6 module)
  * AmgX: 2.5.0? (amgx/2.5.0 module)
  * PETSc: 3.22.2(/work/opt/local/aarch64/apps/gcc/12.4.0/ompi-cuda/4.1.6-12.6/openfoam-gpu/v2406/petsc)
    * Optimize options: -g -O0

## Linear solver for pressure equation

* PETSc-AMG
  * Solver: CG (PETSc)
  * Preconditioner: Boomer AMG (HYPRE)
  * Caching: on(matrix and preconditioner)
* AmgX-AMG
  * Solver: PCG (AmgX)
  * Preconditioner: AMG (AmgX)
  * Smoother: BLOCK JACOBI (AmgX)
  * Base config file: PCG_CLASSICAL_V_JACOBI.json

## Results

### Case S (1M cells)
#### Node vs time
![Node vs time](S/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](S/node-cost.png "Node vs cost")

### Case M (8M cells)
#### Node vs time
![Node vs time](M/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](M/node-cost.png "Node vs cost")

### Case XL (64M cells)
#### Node vs time
![Node vs time](XL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XL/node-cost.png "Node vs cost")

### Case XXL (216M cells)
#### Node vs time
![Node vs time](XXL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XXL/node-cost.png "Node vs cost")
