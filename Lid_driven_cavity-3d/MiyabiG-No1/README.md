# MiyabiG-No1

## General information

* Measurer: IMANO Masashi
* Date: Dec 2024

## Hardware

* Miyabi-G (https://www.cc.u-tokyo.ac.jp/en/supercomputer/miyabi/system.php)

## Cases

* Nvidia-24.9,AmgX-AMG
  * Compiler: Nvidia 24.9 (nvidia/24.9 module)
    * Optimize options: -O3
  * CUDA: CUDA 12.6 (cuda/12.6 module)
  * MPI: OpenMPI 4.1.6(ompi/4.1.6-12.6)
  * AmgX: Git master branch
  * PETSc: 3.22.2
    * Optimize options: -O3

* Gcc-11.4.1,AmgX-AMG
  * Compiler: GCC 11.4.1 (gcc/11.4.1 module)
    * Optimize options: -O3
  * CUDA: 12.6 (cuda/12.6 module)
  * MPI: OpenMPI 4.1.6(ompi/4.1.6-12.6)
  * AmgX: Git master branch
  * PETSc: 3.22.2
    * Optimize options: -O3
  
* Gcc-12.4.0,AmgX-AMG
  * Compiler: GCC 12.4.0 (gcc/12.4.0 module)
    * Optimize options: -O3
  * CUDA: CUDA 12.6 (cuda/12.6 module)
  * MPI: OpenMPI 4.1.6(ompi/4.1.6-12.6)
  * AmgX: Git master branch
  * PETSc: 3.22.2
    * Optimize options: -O3
  
* Gcc-12.4.0(module),AmgX-AMG
  * OpenFOAM: OpenFOAM-v2406 (openfoam-gpu/v2406 module) 
  * Compiler: GCC 12.4.0 (gcc/12.4.0 module)
    * Optimize options: -O3
  * CUDA: 12.6 (cuda/12.6 module)
  * MPI: OpenMPI 4.1.6(ompi/4.1.6-12.6 module)
  * AmgX: 2.5.0? (amgx/2.5.0 module)
  * PETSc: 3.22.2(/work/opt/local/aarch64/apps/gcc/12.4.0/ompi-cuda/4.1.6-12.6/openfoam-gpu/v2406/petsc)
    * Optimize options: -g -O0

## Results

### Case S (1M cells)
#### Node vs time±SD
![Node vs time±SD](S/node-time-sd.png "Node vs time±SD")
#### Node vs time
![Node vs time](S/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](S/node-cost.png "Node vs cost")

### Case M (8M cells)
#### Node vs time±SD
![Node vs time±SD](M/node-time-sd.png "Node vs time±SD")
#### Node vs time
![Node vs time](M/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](M/node-cost.png "Node vs cost")

### Case XL (64M cells)
#### Node vs time±SD
![Node vs time±SD](XL/node-time-sd.png "Node vs time±SD")
#### Node vs time
![Node vs time](XL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XL/node-cost.png "Node vs cost")

### Case XXL (216M cells)
#### Node vs time±SD
![Node vs time±SD](XXL/node-time-sd.png "Node vs time±SD")
#### Node vs time
![Node vs time](XXL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XXL/node-cost.png "Node vs cost")

## How to run

### Make case directories

```bash
./Allrun.pre
```

### Submit jobs

```bash
./Allrun.solve.AmgX
```

### Generate benchmark test results

```bash
./Allrun.time
```

### Plot benchmark test results

```bash
./Allrun.plot
```
