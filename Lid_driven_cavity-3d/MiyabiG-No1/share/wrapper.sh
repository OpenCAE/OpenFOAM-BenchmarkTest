#!/bin/sh
# Perform environmental settings of OpenFOAM
source ${WM_PROJECT_DIR}/etc/bashrc
eval $(foamEtcFile -sh -config petsc -- -force)
export FOAM_SIGFPE=false
export OMP_NUM_THREADS=1
numactl -l $*
