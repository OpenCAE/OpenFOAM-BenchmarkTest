set terminal pdfcairo solid color lw 2 font "Helvetica,13"
set style data linespoints
set logscale x
set logscale y
set format y "10^{%T}"
set grid
set pointsize 0.75
set xlabel "Number of nodes"
set xrange [xmin:xmax]
set xtics (1./4.,1./2.,1,2,4,8) font "Helvetica,11"
set key above font "Courier,10" Left maxcols 2 samplen 4 box linewidth 0.5

set output "node-cost.pdf"
set yrange [*:*]
set ylabel "Cost per step [yen] (<Lower is better)" 
plot \
"< sort -n time/*NvidiaO3-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600.*$1/72.*300000./8640.) title "Nvidia-24.9,AmgX-AMG" with lp lt 1 pt 1\
,"< sort -n time/*Gcc-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600.*$1/72.*300000./8640.) title "Gcc-11.4.1,AmgX-AMG" with lp lt 2 pt 2\
,"< sort -n time/*Gcc12.4.0-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600.*$1/72.*300000./8640.) title "Gcc-12.4.0,AmgX-AMG" with lp lt 3 pt 3\
,"< sort -n time/*GccModule-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600.*$1/72.*300000./8640.) title "Gcc-12.4.0(module),AmgX-AMG" with lp lt 4 pt 4

set label "Linear" at 2*xmin,0.65*timemax rotate by 0 font "Courier Bold,12"
set yrange [*:timemax]
set ylabel "Exectution time per step [h] (<Lower is better)" 

set output "node-time.pdf"
plot \
"< sort -n time/*NvidiaO3-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600) title "Nvidia-24.9,AmgX-AMG" with lp lt 1 pt 1\
,"< sort -n time/*Gcc-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600) title "Gcc-11.4,1,AmgX-AMG" with lp lt 2 pt 2\
,"< sort -n time/*Gcc12.4.0-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600) title "Gcc-12.4.0,AmgX-AMG" with lp lt 3 pt 3\
,"< sort -n time/*GccModule-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600) title "Gcc-12.4.0(module),AmgX-AMG" with lp lt 4 pt 4\
,timemax/(x/xmin) title "" with l lt 1 lc 0 lw 0.5

set output "node-time-sd.pdf"
plot \
"< sort -n time/*NvidiaO3-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600):($4/3600) title "Nvidia-24.9,AmgX-AMG" with errorlines lt 1 pt 1\
,"< sort -n time/*Gcc-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600):($4/3600) title "Gcc-11.4,1,AmgX-AMG" with errorlines lt 2 pt 2\
,"< sort -n time/*Gcc12.4.0-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600):($4/3600) title "Gcc-12.4.0,AmgX-AMG" with errorlines lt 3 pt 3\
,"< sort -n time/*GccModule-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600):($4/3600) title "Gcc-12.4.0(module),AmgX-AMG" with errorlines lt 4 pt 4\
,timemax/(x/xmin) title "" with l lt 1 lc 0 lw 0.5
