#!/bin/bash
for problem in S M XL
do
    echo $problem
    (cd $problem
     ../bin/averageExecutionTime.sh cases/proc_*/cases/*/log.icoFoam.* | sort -k 3,4 -n -t ',' > time.csv
     gnuplot plot.gp
    )
done
