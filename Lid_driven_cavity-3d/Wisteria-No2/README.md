# Wisteria-No2

## General information

* Measurer: IMANO Masashi
* Date: May 2024

## Hardware

* Name: Wisteria/BDEC-01 (https://www.cc.u-tokyo.ac.jp/en/supercomputer/)

## How to run

### Make case directories

```bash
./Allrun.pre
```

### Gerarate meshes

```bash
./Allrun.blockMesh
```

### Decompose regions

```bash
./Allrun.decomposePar
```

### Submit jobs

```bash
./Allrun.solve.CASENAME
```

* CASENAME
  * A64FX
  * AmgX
  * PETSc
  * RapidCFD
