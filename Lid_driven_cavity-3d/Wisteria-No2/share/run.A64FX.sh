#!/bin/sh
module load cmake fftw scotch
source /work/gt00/share/opt/local/aarch64/apps/fj/1.2.39/fjmpi/1.2.39/openfoam/v2312/OpenFOAM-v2312/etc/bashrc
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions
eval $(foamEtcFile -sh -config petsc -- -force)
application=$(getApplication)
jobid=${PJM_SUBJOBID:-$PJM_JOBID}
log=log.$application.$jobid
export OMP_NUM_THREADS=1
env
if [ $PJM_MPI_PROC -eq 1 ]
then
       $application -lib petscFoam &> $log
else
    mpiexec -of $log $application -parallel -lib petscFoam
fi
