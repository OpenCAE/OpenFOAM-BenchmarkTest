#!/bin/sh
ppn=8

# OpenFOAM version
openfoam_version=v2312

# Load dependant modules
module --no-pager load gcc/12.2.0
module --no-pager load cuda/12.0
module --no-pager load ompi-cuda/4.1.6-12.0

# Instal directory
dir=/work/gt00/share/opt/local/x86_64/apps/gcc/12.2.0/cuda/12.0/ompi-cuda/4.1.6-12.0

# Perform environmental settings of OpenFOAM, AmgX, petsc
source $dir/openfoam/$openfoam_version/OpenFOAM-$openfoam_version/etc/bashrc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$dir/amgx/main/lib
eval $(foamEtcFile -sh -config petsc -- -force)

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# Log filename
log=log.$application.$jobid

export OMP_NUM_THREADS=1
env
if [ $PJM_MPI_PROC -eq 1 ]
then
    $application -lib petscFoam &> $log
else
    if [ $PJM_MPI_PROC -lt $ppn ]
    then
	npernode=$PJM_MPI_PROC
    else
	npernode=$ppn
    fi
    mpiexec --report-bindings --display-map -display-devel-map -machinefile $PJM_O_NODEINF -n $PJM_MPI_PROC -npernode $npernode ../../../../../share/wrapper.sh $application -lib petscFoam -parallel &> $log
fi
