#!/bin/sh

# Set variables of precision
WM_PRECISION_OPTION=DP # DP | SP

# Set variables of compile option name
WM_COMPILE_OPTION=Opt

# Versions of Gcc, CUDA and CUDA-aware OpenMPI
gcc_version=8.3.1
cuda_version=12.2
openmpi_version=4.1.5

# Load dependant modules
module --no-pager purge
module --no-pager load gcc/${gcc_version}
module --no-pager load cuda/${cuda_version}
module --no-pager load ompi-cuda/${openmpi_version}-${cuda_version}

# Instal directory
dir=/work/gt00/share/opt/local/x86_64/apps/gcc/${gcc_version}/cuda/${cuda_version}/ompi-cuda/${openmpi_version}-${cuda_version}

# RapidCFD environmental settings
source $dir/rapidcfd/dev/RapidCFD-dev/etc/bashrc WM_PRECISION_OPTION=${WM_PRECISION_OPTION} WM_COMPILE_OPTION=${WM_COMPILE_OPTION}

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# Log filename
log=log.$application.$jobid

deviceArray=("0" "1" "2" "3" "4" "5" "6" "7")
devices="("
i=0
startdevice=0
deviceI=$startdevice
while [ "$i" -lt "$PJM_MPI_PROC" ]
do
    devices="$devices ${deviceArray[$deviceI]}"
    i=$(expr $i + 1)
    deviceI=$(expr $deviceI + 1)
    if [ "$deviceI" -eq 8 ];then
	deviceI=$startdevice
    fi
done
devices="$devices )"

env
if [ $PJM_MPI_PROC -eq 1 ]
then
    $application &> $log
else
    if [ $PJM_MPI_PROC -lt 8 ]
    then
	npernode=$PJM_MPI_PROC
    else
	npernode=8
    fi
    mpiexec\
	--report-bindings\
	--display-map -display-devel-map\
	-machinefile $PJM_O_NODEINF\
	-n $PJM_MPI_PROC\
	-npernode $npernode\
	$application -devices "$devices" -parallel\
	&> $log
fi
