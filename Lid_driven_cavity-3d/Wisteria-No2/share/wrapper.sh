#!/bin/sh
case $OMPI_COMM_WORLD_LOCAL_RANK in
    0|1) export UCX_NET_DEVICES=mlx5_0:1 ;;
    2|3) export UCX_NET_DEVICES=mlx5_1:1 ;;
    4|5) export UCX_NET_DEVICES=mlx5_2:1 ;;
    6|7) export UCX_NET_DEVICES=mlx5_3:1 ;;
esac
export LOCAL_ID=$OMPI_COMM_WORLD_LOCAL_RANK
export CUDA_VISIBLE_DEVICES=$LOCAL_ID
numactl -l $*
