#!/bin/bash
nSteps=$(awk '/^deltaT/ {deltaT=$2} /^endTime/ {endTime=$2} END {print endTime/deltaT}'  controlDict)
echo  "#Log,Host,Node,Proc,Case,TimeSteps,InitTime[s],LastTime[s],Time[s],AveTime[s],SM"
for log in $*
do
    grep "^ExecutionTime" $log >& /dev/null || continue
    jobid=${log##*.}
    sm=$(awk -F ':' 'BEGIN {sm=0} /Multiprocessor count/ {sm+=$2} END {if (sm==0) {sm=132};print sm}' ${log%/*}/*.o${jobid})
    Case=${log%/*}
    Case=${Case##*/}
    awk -v nSteps=$nSteps -v Case=$Case -v Log=$log -v sm=$sm \
'BEGIN {m=0} /Slaves :/ {node=1} /^ *[\("][a-zA-Z0-9]*[ \.][0-9]*[\)"]$/ {node++} /^Host   : mg/ {Host="Miyabi-G"} /^Host   : mc/ {Host="Miyabi-C"} /^nProcs : / {nProcs=$3} /^Time =/ {n++} /^ExecutionTime / {if (n==1) {t0=$3;} else if (n==nSteps) {t1=$3}} \
END {if (t1>0) {node==0 && node=1;printf "%s,%s,%d,%d,%s,%d,%g,%g,%g,%g,%d\n",Log,Host,node,nProcs,Case,nSteps,t0,t1,t1-t0,(t1-t0)/(n-1),sm}}' $log
done
