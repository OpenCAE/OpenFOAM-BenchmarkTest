#!/bin/bash

atexit () {
    trap - EXIT
    if [ $OMPI_COMM_WORLD_LOCAL_RANK -eq 0 ]; then
	# Quit cuda MPS if it's running
	ps aux | grep nvidia-cuda-mps-control | grep -v grep > /dev/null
	if [ $? -eq 0 ]; then
	    echo quit | /usr/bin/nvidia-cuda-mps-control
	fi
	rm -rf "$tmpdir"
    fi
}

if [ $OMPI_COMM_WORLD_LOCAL_RANK -eq 0 ]; then
    tmpdir=$(mktemp -d)
    trap atexit EXIT HUP INT QUIT TERM USR1 USR2
    export CUDA_MPS_PIPE_DIRECTORY=$tmpdir/nvidia-mps
    export CUDA_MPS_LOG_DIRECTORY=$tmpdir/nvidia-log
    /usr/bin/nvidia-cuda-mps-control -d
    echo "start_server -uid $(id -u)" | /usr/bin/nvidia-cuda-mps-control
fi

$@
