# Perform environmental settings of OpenFOAM
module purge
module load gcc/12.4.0 cuda/12.6 ompi-cuda/4.1.6-12.6 amgx/2.5.0 openfoam-gpu/v2406
source ${WM_PROJECT_DIR}/etc/bashrc
eval $(foamEtcFile -sh -config petsc -- -force)
export FOAM_SIGFPE=false
export OMP_NUM_THREADS=1

# Get application name
application=icoFoam

# Job ID
jobid=${PBS_JOBID}
jobid=${jobid%.opbs}

# Log filename
log=log.$application.$jobid

# Print informations
for command in \
    'numactl -s' \
	'cat /proc/cpuinfo' \
	'nvidia-smi -q' \
	'ompi_info' \
	'env'
do
  echo -e "\n${command}\n---"
  ${command}
done

# Exit if small MIGs exist
[ $MPI_PROC_PER_NODE -ne 72 ] && nvidia-smi -q | grep 'Multiprocessor count *: 26' &> /dev/null && exit 0

# Run solver
MPI_PROC=`wc -l $PBS_NODEFILE | awk '{print $1}'`
NODE_NUM=`sort -u $PBS_NODEFILE | wc -l`
MPI_PROC_PER_NODE=`expr $MPI_PROC / $NODE_NUM`

cd ${PBS_O_WORKDIR}
cp -a ../../../../../share/mps.sh ./
if [ ${MPI_PROC} -eq 1 ]
then
    command="numactl -l ${application} -lib petscFoam"
else
    unset OMPI_MCA_mca_base_env_list
    command="mpirun --bind-to core:overload-allowed -x PATH -x LD_LIBRARY_PATH -x WM_PROJECT_DIR numactl -l ${application} -lib petscFoam -parallel"
fi
echo -e "Execute command\n---\n${command}"
${command} &> ${log}
