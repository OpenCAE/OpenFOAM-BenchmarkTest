r=10./8.
xmin=xmin/r
xmax=xmax*r
set terminal pdfcairo solid color lw 2 font "Helvetica,13"
set style data linespoints
set logscale x
set logscale y
set format y "10^{%T}"
set grid
set pointsize 0.75
set xlabel "Number of nodes"
set xrange [xmin:xmax]
set xtics (1./4.,1./2.,1,2,4,8,16,32,64,128,256) font "Helvetica,11"
set key above font "Courier,10" Left maxcols 2 samplen 4 box linewidth 0.5

set output "node-cost.pdf"
set yrange [*:*]
set ylabel "Cost per step [yen] (<Lower is better)" 
plot \
"< awk '{if ($1<72) {print $0}}' time/migNvidiaO3-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=72) {print $0}}' time/NvidiaO3-AmgX-PCG_CLASSICAL_V_JACOBI" using ($1/72.0):($3/3600.*$1/72.*300000./8640.) title "Nvidia-24.9,AmgX-AMG" with lp lt 1 pt 1\
,"< awk '{if ($1<72) {print $0}}' time/migmpsNvidiaO3-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=72) {print $0}}' time/mpsNvidiaO3-AmgX-PCG_CLASSICAL_V_JACOBI" using ($1/72.0):($3/3600.*$1/72.*300000./8640.) title "Nvidia-24.9,AmgX-AMG,MPS" with lp lt 2 pt 2\
,"< awk '{if ($1<72) {print $0}}' time/migGccModulePETScO3-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=72) {print $0}}' time/GccModulePETScO3-AmgX-PCG_CLASSICAL_V_JACOBI" using ($1/72.0):($3/3600.*$1/72.*300000./8640.) title "Gcc-12.4.0,AmgX-AMG" with lp lt 4 pt 4\
,"< awk '{if ($1<72) {print $0}}' time/migmpsGccModulePETScO3-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=72) {print $0}}' time/mpsGccModulePETScO3-AmgX-PCG_CLASSICAL_V_JACOBI" using ($1/72.0):($3/3600.*$1/72.*300000./8640.) title "Gcc-12.4.0,AmgX-AMG,MPS" with lp lt 6 pt 6\

set label "Linear" at 2*xmin,0.65*timemax rotate by 0 font "Courier Bold,12"
set yrange [*:timemax]
set ylabel "Exectution time per step [h] (<Lower is better)" 

set output "node-time.pdf"
plot \
"< awk '{if ($1<72) {print $0}}' time/migNvidiaO3-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=72) {print $0}}' time/NvidiaO3-AmgX-PCG_CLASSICAL_V_JACOBI" using ($1/72.0):($3/3600) title "Nvidia-24.9,AmgX-AMG" with lp lt 1 pt 1\
,"< awk '{if ($1<72) {print $0}}' time/migmpsNvidiaO3-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=72) {print $0}}' time/mpsNvidiaO3-AmgX-PCG_CLASSICAL_V_JACOBI" using ($1/72.0):($3/3600) title "Nvidia-24.9,AmgX-AMG,MPS" with lp lt 2 pt 2\
,"< awk '{if ($1<72) {print $0}}' time/migGccModulePETScO3-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=72) {print $0}}' time/GccModulePETScO3-AmgX-PCG_CLASSICAL_V_JACOBI" using ($1/72.0):($3/3600) title "Gcc-12.4.0,AmgX-AMG" with lp lt 4 pt 4\
,"< awk '{if ($1<72) {print $0}}' time/migmpsGccModulePETScO3-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=72) {print $0}}' time/mpsGccModulePETScO3-AmgX-PCG_CLASSICAL_V_JACOBI" using ($1/72.0):($3/3600) title "Gcc-12.4.0,AmgX-AMG,MPS" with lp lt 6 pt 6\
,timemax/(x/xmin) title "" with l lt 1 lc 0 lw 0.5

set output "node-time-sd.pdf"
plot \
"< awk '{if ($1<72) {print $0}}' time/migNvidiaO3-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=72) {print $0}}' time/NvidiaO3-AmgX-PCG_CLASSICAL_V_JACOBI" using ($1/72.0):($3/3600):($4/3600) title "Nvidia-24.9,AmgX-AMG" with errorlines lt 1 pt 1\
,"< awk '{if ($1<72) {print $0}}' time/migmpsNvidiaO3-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=72) {print $0}}' time/mpsNvidiaO3-AmgX-PCG_CLASSICAL_V_JACOBI" using ($1/72.0):($3/3600):($4/3600) title "Nvidia-24.9,AmgX-AMG,MPS" with errorlines lt 2 pt 2\
,"< awk '{if ($1<72) {print $0}}' time/migGccModulePETScO3-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=72) {print $0}}' time/GccModulePETScO3-AmgX-PCG_CLASSICAL_V_JACOBI" using ($1/72.0):($3/3600):($4/3600) title "Gcc-12.4.0,AmgX-AMG" with errorlines lt 4 pt 4\
,"< awk '{if ($1<72) {print $0}}' time/migmpsGccModulePETScO3-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=72) {print $0}}' time/mpsGccModulePETScO3-AmgX-PCG_CLASSICAL_V_JACOBI" using ($1/72.0):($3/3600):($4/3600) title "Gcc-12.4.0,AmgX-AMG,MPS" with errorlines lt 6 pt 6\
,timemax/(x/xmin) title "" with l lt 1 lc 0 lw 0.5
