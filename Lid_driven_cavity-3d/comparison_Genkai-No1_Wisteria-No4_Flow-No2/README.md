# comparison_Genkai-No1_Wisteria-No4_Flow-No2

## Hardware

* Genkai (https://www.cc.kyushu-u.ac.jp/scp/system/Genkai/)
* Wisteria/BDEC-01 Aquarius, Odyssey (https://www.cc.u-tokyo.ac.jp/en/supercomputer/wisteria/system.php)
* "Flow" Type II (https://icts.nagoya-u.ac.jp/ja/sc/overview.html)

## Results

### Case S (1M cells)
#### Node vs time
![Node vs time](S/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](S/node-cost.png "Node vs cost")

### Case M (8M cells)
#### Node vs time
![Node vs time](M/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](M/node-cost.png "Node vs cost")

### Case XL (64M cells)
#### Node vs time
![Node vs time](XL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XL/node-cost.png "Node vs cost")

### Case XXL (216M cells)
#### Node vs time
![Node vs time](XXL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XXL/node-cost.png "Node vs cost")
