#!/bin/sh
# Perform environmental settings of OpenFOAM
prefix=${HOME}/opt/local;openfoam_version=v2406;intel_version=2023.2.0;impi_version=2021.10.0;WM_COMPILER=Icx-${intel_version};WM_MPLIB=INTELMPI-${impi_version};module --no-pager purge;module --no-pager load intel/${intel_version} impi/${impi_version};machine=$(uname -m);dir=${prefix}/${machine}/apps/intel/${intel_version}/impi/${impi_version};openfoam_dir=${dir}/openfoam/${openfoam_version};source ${openfoam_dir}/OpenFOAM-${openfoam_version}/etc/bashrc WM_COMPILER=${WM_COMPILER} WM_MPLIB=${WM_MPLIB};eval $(foamEtcFile -sh -config petsc -- -force)
export FOAM_SIGFPE=false
export OMP_NUM_THREADS=1
numactl -l $*
