# MiyabiC-No1

## General information

* Measurer: IMANO Masashi
* Date: Dec 2024

## Hardware

* Miyabi-C (https://www.cc.u-tokyo.ac.jp/en/supercomputer/miyabi/system.php)

## Results

### Case S (1M cells)
#### Node vs time±SD
![Node vs time±SD](S/node-time-sd.png "Node vs time±SD")
#### Node vs time
![Node vs time](S/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](S/node-cost.png "Node vs cost")

### Case M (8M cells)
#### Node vs time±SD
![Node vs time±SD](M/node-time-sd.png "Node vs time±SD")
#### Node vs time
![Node vs time](M/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](M/node-cost.png "Node vs cost")

### Case XL (64M cells)
#### Node vs time±SD
![Node vs time±SD](XL/node-time-sd.png "Node vs time±SD")
#### Node vs time
![Node vs time](XL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XL/node-cost.png "Node vs cost")

### Case XXL (216M cells)
#### Node vs time±SD
![Node vs time±SD](XXL/node-time-sd.png "Node vs time±SD")
#### Node vs time
![Node vs time](XXL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XXL/node-cost.png "Node vs cost")

## How to run

### Make case directories

```bash
./Allrun.pre
```

### Submit jobs

```bash
./Allrun.solve
```

### Generate benchmark test results

```bash
./Allrun.time
```

### Plot benchmark test results

```bash
./Allrun.plot
```
