#!/bin/sh
ppn=$1
shift
gpus_per_node=$1
shift
export CUDA_VISIBLE_DEVICES=$((${OMPI_COMM_WORLD_LOCAL_RANK} % ${gpus_per_node}))
(
    echo CUDA_VISIBLE_DEVICES: ${CUDA_VISIBLE_DEVICES}
    echo
    numactl -s
    echo
    env
) > ${0##*/}.${PJM_JOBID}.${OMPI_COMM_WORLD_RANK}.out
numactl -l $*
