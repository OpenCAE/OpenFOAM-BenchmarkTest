set terminal pdfcairo solid color lw 2 font "Helvetica,13"
set style data linespoints
set logscale x
set logscale y
set format y "10^{%T}"
set grid
set pointsize 0.75
set xlabel "Number of nodes"
set xrange [xmin:xmax]
set xtics (1./8.,1./4.,1./2.,1,2,3,4,6,8,12,16,24,32,48,64,96,128,192) font "Helvetica,11"
set key above font "Courier,10" Left maxcols 2 samplen 4 box linewidth 0.5

set output "node-cost.pdf"
set yrange [*:*]
set ylabel "Cost per step [yen] (<Lower is better)" 
plot \
"< sort -n time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/40):($3*$1/40.*4.*0.007/0.65) title " V100,AmgX-AMG" with lp lt 1 pt 1

set label "Linear" at 2*xmin,0.65*timemax rotate by 0 font "Courier Bold,12"
set output "node-time.pdf"
set yrange [*:timemax]
set ylabel "Exectution time per step [h] (<Lower is better)" 
plot \
"< sort -n time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/40):($3/3600) title " V100,AmgX-AMG" with lp lt 1 pt 1\
,timemax/(x/xmin) title "" with l lt 1 lc 0 lw 0.5
