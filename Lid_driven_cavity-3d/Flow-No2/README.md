# Flow-No2

## General information

* Measurer: IMANO Masashi
* Date: Jul 2024

## Hardware

* "Flow" Type II (https://icts.nagoya-u.ac.jp/ja/sc/overview.html)

## Results

### Case S (1M cells)
#### Node vs time
![Node vs time](S/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](S/node-cost.png "Node vs cost")

### Case M (8M cells)
#### Node vs time
![Node vs time](M/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](M/node-cost.png "Node vs cost")

### Case XL (64M cells)
#### Node vs time
![Node vs time](XL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XL/node-cost.png "Node vs cost")

### Case XXL (216M cells)
#### Node vs time
![Node vs time](XXL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XXL/node-cost.png "Node vs cost")

## How to run

### Make case directories

```bash
./Allrun.pre
```

### Gerarate meshes

```bash
./Allrun.blockMesh
```

### Decompose regions

```bash
./Allrun.decomposePar
```

### Submit jobs

```bash
./Allrun.solve.AmgX
```

### Generate benchmark test results

```bash
./Allrun.time
```

### Plot benchmark test results

```bash
./Allrun.plot
```
