#!/bin/sh
#PJM -L rscunit=cx
#PJM -L rg=cx-small
#PJM -L node=1
#PJM -L elapse=24:00:00
#PJM -j
#PJM -S
prefix=/data/group2/others/opt/local;openfoam_version=v2406;gcc_version=11.3.0;cuda_version=12.1.1;openmpi_cuda_version=4.0.5;module load gcc/${gcc_version} cuda/${cuda_version} openmpi_cuda/${openmpi_cuda_version};dir=${prefix}/x86_64/apps/gcc/${gcc_version}/cuda/${cuda_version}/openmpi_cuda/${openmpi_cuda_version};source ${dir}/openfoam/${openfoam_version}/OpenFOAM-${openfoam_version}/etc/bashrc
decomposePar &> log.decomposePar
