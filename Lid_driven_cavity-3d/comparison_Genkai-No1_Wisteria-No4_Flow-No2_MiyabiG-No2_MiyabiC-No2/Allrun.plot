#!/bin/bash
for problem in S M XL XXL
do
    echo $problem
    (cd $problem
     gnuplot plot.gp
     for f in *.pdf
     do
	 magick -density 150 $f ${f/.pdf/.png}
     done
    )
done
