set terminal pdfcairo solid color lw 2 font "Helvetica,13"
set style data linespoints
set logscale x
set logscale y
set format y "10^{%T}"
set grid
set pointsize 0.75
set xlabel "Number of nodes"
set xrange [xmin:xmax]
set xtics (1./8.,1./4.,1./2.,1,2,4,8,16,32,64,128) font "Helvetica,11"
set key above font "Courier,9" Left maxcols 3 samplen 4 box linewidth 0.5

set output "node-cost.pdf"
set yrange [*:*]
set ylabel "Cost per step [yen] (<Lower is better)" 
plot \
 "< awk '{if ($2<=32) {print $0}}' MiyabiC/time/Icx-FOAM-DIC-PCG.fixedNORM" using ($2):($3/3600.*$2*300000./8640.*0.8) title "Max9480,PCG-DIC" with lp lt 4 pt 4\
,"< awk '{if ($2<=32) {print $0}}' MiyabiC/time/Icx-FOAM-GAMG-PCG.fixedNORM" using ($2):($3/3600.*$2*300000./8640.*0.8) title "Max9480,PCG-GAMG" with lp lt 6 pt 6\
,"< awk '{if ($2<=32) {print $0}}' MiyabiC/time/Icx-PETSc-ICC-CG.mpiaij.fixedNORM" using ($2):($3/3600.*$2*300000./8640.*0.8) title "Max9480,PETSc-ICC" with lp lt 8 pt 8\
,"< awk '{if ($2<=32) {print $0}}' MiyabiC/time/Icx-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using ($2):($3/3600.*$2*300000./8640.*0.8) title "Max9480,PETSc-AMG" with lp lt 10 pt 10\
,"< sort -n Genkai/time/*cpuGccN-*-FOAM-DIC-PCG.fixedNORM" using ($1/120):($3/3600.*$1/120.*30.) title "8490H,PCG-DIC" with lp lt 5 pt 5\
,"< sort -n Genkai/time/*cpuGccN-*-FOAM-GAMG-PCG.fixedNORM" using ($1/120):($3/3600.*$1/120.*30.) title "8490H,PCG-GAMG" with lp lt 7 pt 7\
,"< sort -n Genkai/time/*cpuGccN-*-PETSc-ICC-CG.mpiaij.fixedNORM" using ($1/120):($3/3600.*$1/120.*30.) title "8490H,PETSc-ICC" with lp lt 9 pt 9\
,"< sort -n Genkai/time/*cpuGccN-*-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using ($1/120):($3/3600.*$1/120.*30.) title "8490H,PETSc-AMG" with lp lt 11 pt 11\
,"< sort -n Flow/time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/40):($3*$1/40.*4.*0.007/0.65) title " V100,AmgX-AMG" with lp lt 1 pt 1\
,"< sort -n Wisteria/time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600.*$1/72.*90000.*3.*8./8640.) title " A100,AmgX-AMG" with lp lt 2 pt 2\
,"< sort -n Genkai/time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/120):($3/3600.*$1/120.*120.) title " H100,AmgX-AMG" with lp lt 3 pt 3\
,"< (awk '{if ($1>=72) {print $0}}' MiyabiG/time/GccModule-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=18) {print $0}}' MiyabiG/time/migGccModule-AmgX-PCG_CLASSICAL_V_JACOBI) | sort -n" using ($1/72):($3/3600.*$1/72.*300000./8640.) title " GH200,AmgX-AMG" with lp lt 12 pt 12\

set label "Linear" at 2*xmin,0.65*timemax rotate by 0 font "Courier Bold,12"
set output "node-time.pdf"
set yrange [*:timemax]
set ylabel "Exectution time per step [h] (<Lower is better)" 
plot \
 "< awk '{if ($2<=32) {print $0}}' MiyabiC/time/Icx-FOAM-DIC-PCG.fixedNORM" using ($1/112):($3/3600) title "Max9480,PCG-DIC" with lp lt 4 pt 4\
,"< awk '{if ($2<=32) {print $0}}' MiyabiC/time/Icx-FOAM-GAMG-PCG.fixedNORM" using ($1/112):($3/3600) title "Max9480,PCG-GAMG" with lp lt 6 pt 6\
,"< awk '{if ($2<=32) {print $0}}' MiyabiC/time/Icx-PETSc-ICC-CG.mpiaij.fixedNORM" using ($1/112):($3/3600) title "Max9480,PETSc-ICC" with lp lt 8 pt 8\
,"< awk '{if ($2<=32) {print $0}}' MiyabiC/time/Icx-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using ($1/112):($3/3600) title "Max9480,PETSc-AMG" with lp lt 10 pt 10\
,"< sort -n Genkai/time/*cpuGccN-*-FOAM-DIC-PCG.fixedNORM" using ($1/120):($3/3600) title "8490H,PCG-DIC" with lp lt 5 pt 5\
,"< sort -n Genkai/time/*cpuGccN-*-FOAM-GAMG-PCG.fixedNORM" using ($1/120):($3/3600) title "8490H,PCG-GAMG" with lp lt 7 pt 7\
,"< sort -n Genkai/time/*cpuGccN-*-PETSc-ICC-CG.mpiaij.fixedNORM" using ($1/120):($3/3600) title "8490H,PETSc-ICC" with lp lt 9 pt 9\
,"< sort -n Genkai/time/*cpuGccN-*-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using ($1/120):($3/3600) title "8490H,PETSc-AMG" with lp lt 11 pt 11\
,"< sort -n Flow/time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/40):($3/3600) title " V100,AmgX-AMG" with lp lt 1 pt 1\
,"< sort -n Wisteria/time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/72):($3/3600) title " A100,AmgX-AMG" with lp lt 2 pt 2\
,"< sort -n Genkai/time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI " using ($1/120):($3/3600) title " H100,AmgX-AMG" with lp lt 3 pt 3\
,"< (awk '{if ($1>=72) {print $0}}' MiyabiG/time/GccModule-AmgX-PCG_CLASSICAL_V_JACOBI;awk '{if ($1>=18) {print $0}}' MiyabiG/time/migGccModule-AmgX-PCG_CLASSICAL_V_JACOBI) | sort -n" using ($1/72):($3/3600) title " GH200,AmgX-AMG" with lp lt 12 pt 12\
,timemax/(x/xmin) title "" with l lt 1 lc 0 lw 0.5
