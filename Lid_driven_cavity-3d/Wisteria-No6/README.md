# Wisteria-No6

## General information

* Measurement
  * Measurer: IMANO Masashi
  * Measurement date: Feb 2025
* System and software
  * System: Wisteria/BDEC-01 Odyssey (https://www.cc.u-tokyo.ac.jp/en/supercomputer/)
  * OpenFOAM: v2406
  * Compiler: Fujitsu Compiler 1.2.39 (fj/1.2.39 module)
    * Optimize options: -O3
  * MPI: Fujitsu MPI 1.2.39 (fjmpi/1.2.39 module)

## Linear solver for pressure equation

* PCG-DIC
  * Solver: PCG (OpenFOAM)
  * Preconditioner: DIC (OpenFOAM)
* PCG-GAMG
  * Solver: PCG (OpenFOAM)
  * Preconditioner: GAMG (OpenFOAM)
  * Smoother: GaussSeidel (OpenFOAM)
* PETSc-ICC
  * Solver: CG (PETSc)
  * Preconditioner: BLOCK JACOBI (PETSc)
  * Sub preconditioner: ICC (PETSc)
* PETSc-AMG
  * Solver: CG (PETSc)
  * Preconditioner: Boomer AMG (HYPRE)
  * Caching: off
* PETSc-AMG(caching)
  * Solver: CG (PETSc)
  * Preconditioner: Boomer AMG (HYPRE)
  * Caching: on(matrix and preconditioner)

## Results

### Case S (1M cells)
#### Node vs time±SD
![Node vs time±SD](S/node-time-sd.png "Node vs time±SD")
#### Node vs time
![Node vs time](S/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](S/node-cost.png "Node vs cost")

### Case M (8M cells)
#### Node vs time±SD
![Node vs time±SD](M/node-time-sd.png "Node vs time±SD")
#### Node vs time
![Node vs time](M/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](M/node-cost.png "Node vs cost")

### Case XL (64M cells)
#### Node vs time±SD
![Node vs time±SD](XL/node-time-sd.png "Node vs time±SD")
#### Node vs time
![Node vs time](XL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XL/node-cost.png "Node vs cost")

### Case XXL (216M cells)
#### Node vs time±SD
![Node vs time±SD](XXL/node-time-sd.png "Node vs time±SD")
#### Node vs time
![Node vs time](XXL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XXL/node-cost.png "Node vs cost")

## How to run

### Make case directories

```bash
./Allrun.pre
```

### Submit jobs

```bash
./Allrun.solve
```

### Generate benchmark test results

```bash
./Allrun.time
```

### Plot benchmark test results

```bash
./Allrun.plot
```
