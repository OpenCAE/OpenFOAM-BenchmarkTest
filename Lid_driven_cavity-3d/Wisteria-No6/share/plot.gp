set terminal pdfcairo solid color lw 2 font "Helvetica,13"
set style data linespoints
set logscale x
set logscale y
set format y "10^{%T}"
set grid
set pointsize 0.75
set xlabel "Number of nodes"
set xrange [xmin:xmax]
set xtics (1,2,3,4,6,8,12,16,24,32,48,64,96,128,192,384) font "Helvetica,11"
set key above font "Courier,10" Left maxcols 2 samplen 4 box linewidth 0.5

set output "node-cost.pdf"
set yrange [*:*]
set ylabel "Cost per step [yen] (<Lower is better)" 
plot \
 "time/v2406-FOAM-DIC-PCG.fixedNORM" using 2:($3/3600.*$2*90000./8640.) title "PCG-DIC  " with lp lt 4 pt 4\
,"time/v2406-FOAM-GAMG-PCG.fixedNORM" using 2:($3/3600.*$2*90000./8640.) title "PCG-GAMG " with lp lt 6 pt 6\
,"time/v2406-PETSc-ICC-CG.mpiaij.fixedNORM" using 2:($3/3600.*$2*90000./8640.) title "PETSc-ICC" with lp lt 8 pt 8\
,"time/v2406-PETSc-AMG-CG.mpiaij.fixedNORM" using 2:($3/3600.*$2*90000./8640.) title "PETSc-AMG" with lp lt 10 pt 10\
,"time/v2406-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using 2:($3/3600.*$2*90000./8640.) title "PETSc-AMG(caching)" with lp lt 12 pt 12\

set label "Linear" at 2*xmin,0.65*timemax rotate by 0 font "Courier Bold,12"
set yrange [*:timemax]
set ylabel "Exectution time per step [h] (<Lower is better)" 

set output "node-time.pdf"
plot \
 "time/v2406-FOAM-DIC-PCG.fixedNORM" using 2:($3/3600):($4/3600) title "PCG-DIC   " with lp lt 4 pt 4\
,"time/v2406-FOAM-GAMG-PCG.fixedNORM" using 2:($3/3600):($4/3600) title "PCG-GAMG  " with lp lt 6 pt 6\
,"time/v2406-PETSc-ICC-CG.mpiaij.fixedNORM" using 2:($3/3600):($4/3600) title "PETSc-ICC " with lp lt 8 pt 8\
,"time/v2406-PETSc-AMG-CG.mpiaij.fixedNORM" using 2:($3/3600):($4/3600) title "PETSc-AMG " with lp lt 10 pt 10\
,"time/v2406-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using 2:($3/3600):($4/3600) title "PETSc-AMG(caching) " with lp lt 12 pt 12\
,timemax/(x/xmin) title "" with l lt 1 lc 0 lw 0.5

set output "node-time-sd.pdf"
plot \
 "time/v2406-FOAM-DIC-PCG.fixedNORM" using 2:($3/3600):($4/3600) title "PCG-DIC   " with errorlines lt 4 pt 4\
,"time/v2406-FOAM-GAMG-PCG.fixedNORM" using 2:($3/3600):($4/3600) title "PCG-GAMG  " with errorlines lt 6 pt 6\
,"time/v2406-PETSc-ICC-CG.mpiaij.fixedNORM" using 2:($3/3600):($4/3600) title "PETSc-ICC " with errorlines lt 8 pt 8\
,"time/v2406-PETSc-AMG-CG.mpiaij.fixedNORM" using 2:($3/3600):($4/3600) title "PETSc-AMG " with errorlines lt 10 pt 10\
,"time/v2406-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using 2:($3/3600):($4/3600) title "PETSc-AMG(caching) " with errorlines lt 12 pt 12\
,timemax/(x/xmin) title "" with l lt 1 lc 0 lw 0.5
