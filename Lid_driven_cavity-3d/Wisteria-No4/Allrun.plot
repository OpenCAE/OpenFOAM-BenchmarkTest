#!/bin/bash
for problem in S M XL XXL
do
    echo $problem
    (cd $problem
        gnuplot plot.gp
	for file in *.pdf
	do
	    magick -density 150 ${file} ${file/.pdf/.png}
	done
    )
done
