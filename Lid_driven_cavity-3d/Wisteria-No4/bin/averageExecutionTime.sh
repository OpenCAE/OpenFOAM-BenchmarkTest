#!/bin/bash
nSteps=$(awk '/^deltaT/ {deltaT=$2} /^endTime/ {endTime=$2} END {print endTime/deltaT}'  controlDict)
echo  "#Directory,Host,Node,Proc,Case,TimeSteps,InitTime[s],LastTime[s],Time[s],AveTime[s]"
for log in $*
do
    grep "^ExecutionTime" $log >& /dev/null || continue
    jobid=${log##*.}
    path=${log%/*}
    Case=${path##*/}
    awk -v nSteps=${nSteps} -v path=${path} -v Case=${Case} \
'BEGIN {m=0} \
/Slaves :/ {node=1} \
/^ *[\("][a-zA-Z0-9]*[ \.][0-9]*[\)"]$/ {node++} \
/^Host   : .*wa/ {Host="Aquarius"} \
/^Host   : .*wo/ {Host="Odyssey"} \
/^nProcs : / {nProcs=$3} \
/^Time =/ {n++} \
/Initializing PETSc/ {m=-1} \
/Number of GPU devices/ {m=0} \
/^ExecutionTime / {m++;if ((n==1) && (m==1)) {t0=$3;} else if (n==nSteps) {t1=$3}} \
END {if (t1>0) {node==0 && node=1;printf "%s,%s,%d,%d,%s,%d,%g,%g,%g,%g\n",path,Host,node,nProcs,Case,nSteps,t0,t1,t1-t0,(t1-t0)/(n-1)}}' \
$log 
done
