set terminal pdfcairo solid color lw 2 font "Helvetica,13"
set style data linespoints
set logscale x
set logscale y
set format y "10^{%T}"
set grid
set pointsize 0.75
set xlabel "Number of nodes"
set xrange [xmin:xmax]
set xtics (1./4.,1./2.,1,2,4,8,16,32,64,128,256) font "Helvetica,11"
set key above font "Courier,10" Left maxcols 2 samplen 4 box linewidth 0.5

set output "node-cost.pdf"
set yrange [*:*]
set ylabel "Cost per step [yen] (<Lower is better)" 
plot \
 "time/Icx-FOAM-DIC-PCG.fixedNORM" using ($2):($3/3600.*$2*300000./8640.*0.8) title "PCG-DIC  " with lp lt 4 pt 4\
,"time/Icx-FOAM-GAMG-PCG.fixedNORM" using ($2):($3/3600.*$2*300000./8640.*0.8) title "PCG-GAMG " with lp lt 6 pt 6\
,"time/Icx-PETSc-ICC-CG.mpiaij.fixedNORM" using ($2):($3/3600.*$2*300000./8640.*0.8) title "PETSc-ICC" with lp lt 8 pt 8\
,"time/Icx-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using ($2):($3/3600.*$2*300000./8640.*0.8) title "PETSc-AMG" with lp lt 10 pt 10\

set label "Linear" at 2*xmin,0.65*timemax rotate by 0 font "Courier Bold,12"
set yrange [*:timemax]
set ylabel "Exectution time per step [h] (<Lower is better)" 

set output "node-time.pdf"
plot \
 "time/Icx-FOAM-DIC-PCG.fixedNORM" using ($1/112):($3/3600):($4/3600) title "PCG-DIC   " with lp lt 4 pt 4\
,"time/Icx-FOAM-GAMG-PCG.fixedNORM" using ($1/112):($3/3600):($4/3600) title "PCG-GAMG  " with lp lt 6 pt 6\
,"time/Icx-PETSc-ICC-CG.mpiaij.fixedNORM" using ($1/112):($3/3600):($4/3600) title "PETSc-ICC " with lp lt 8 pt 8\
,"time/Icx-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using ($1/112):($3/3600):($4/3600) title "PETSc-AMG " with lp lt 10 pt 10\
,timemax/(x/xmin) title "" with l lt 1 lc 0 lw 0.5

set output "node-time-sd.pdf"
plot \
 "time/Icx-FOAM-DIC-PCG.fixedNORM" using ($1/112):($3/3600):($4/3600) title "PCG-DIC   " with errorlines lt 4 pt 4\
,"time/Icx-FOAM-GAMG-PCG.fixedNORM" using ($1/112):($3/3600):($4/3600) title "PCG-GAMG  " with errorlines lt 6 pt 6\
,"time/Icx-PETSc-ICC-CG.mpiaij.fixedNORM" using ($1/112):($3/3600):($4/3600) title "PETSc-ICC " with errorlines lt 8 pt 8\
,"time/Icx-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using ($1/112):($3/3600):($4/3600) title "PETSc-AMG " with errorlines lt 10 pt 10\
,timemax/(x/xmin) title "" with l lt 1 lc 0 lw 0.5
