#!/bin/bash
nSteps=$(awk '/^deltaT/ {deltaT=$2} /^endTime/ {endTime=$2} END {print endTime/deltaT}'  controlDict)
echo  "#Node,Proc,Case,Path,Host,TimeSteps,InitTime[s],LastTime[s],Time[s],AveTime[s]"
for Filename in $*
do
    grep "^ExecutionTime" $Filename >& /dev/null || continue
    Path=${Filename%/*}
    Case=${Path##*/}
    awk -v nSteps=${nSteps} -v Path=${Path} -v Case=${Case} \
'BEGIN {m=0} /Slaves :/ {node=1} /^ *[\("][a-zA-Z0-9]*[ \.][0-9]*[\)"]$/ {node++} /^Host   : a/ {Host="A"} /^Host   : b/ {Host="B"} /^Host   : c/ {Host="C"} /^nProcs : / {nProcs=$3} /^Time =/ {n++} /Initializing PETSc/ {m=-1} /Number of GPU devices/ {m=0} /^ExecutionTime / {m++;if ((n==1) && (m==1)) {t0=$3;} else if (n==nSteps) {t1=$3}} \
END {if (t1>0) {node==0 && node=1;printf "%d,%d,%s,%s,%s,%d,%g,%g,%g,%g\n",node,nProcs,Case,Path,Host,nSteps,t0,t1,t1-t0,(t1-t0)/(n-1)}}' \
$Filename
done
