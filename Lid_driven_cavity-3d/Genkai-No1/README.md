# Genkai-No1

## General information

* Measurer: IMANO Masashi
* Date: July 2024-September 2024

## Hardware

* Name: Genkai (https://www.cc.kyushu-u.ac.jp/scp/system/Genkai/)

## How to run

### Make case directories

```bash
./Allrun.pre
```

### Gerarate meshes

```bash
./Allrun.blockMesh
```

### Decompose regions

```bash
./Allrun.decomposePar
```

### Submit jobs to node group A

```bash
./Allrun.solve.CPU
```

### Submit jobs to node group B

```bash
./Allrun.solve.AmgX
```
### Plot results

```bash
./Allrun.csv
./Allrun.plot
./Allrun.png
```

## Results

### Case S (1M cells)
#### Node vs time
![Node vs time](S/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](S/node-cost.png "Node vs cost")

### Case M (8M cells)
#### Node vs time
![Node vs time](M/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](M/node-cost.png "Node vs cost")

### Case XL (64M cells)
#### Node vs time
![Node vs time](XL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XL/node-cost.png "Node vs cost")

### Case XXL (216M cells)
#### Node vs time
![Node vs time](XXL/node-time.png "Node vs time")
#### Node vs cost
![Node vs cost](XXL/node-cost.png "Node vs cost")
