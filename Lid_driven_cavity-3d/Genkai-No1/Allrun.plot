#!/bin/bash
for problem in S M XL XXL
do
    echo $problem
    (cd $problem
     rm -rf time
     mkdir time
     for Case in \
	 sharecpuGcc-30-FOAM-DIC-PCG.fixedNORM \
	     sharecpuGcc-30-FOAM-GAMG-PCG.fixedNORM \
	     sharecpuGcc-30-PETSc-AMG-CG.mpiaij.fixedNORM \
	     sharecpuGcc-30-PETSc-AMG-CG.mpiaij.fixedNORM.caching \
	     sharecpuGcc-30-PETSc-ICC-CG.mpiaij.fixedNORM \
	     sharecpuGcc-60-FOAM-DIC-PCG.fixedNORM \
	     sharecpuGcc-60-FOAM-GAMG-PCG.fixedNORM \
	     sharecpuGcc-60-PETSc-AMG-CG.mpiaij.fixedNORM \
	     sharecpuGcc-60-PETSc-AMG-CG.mpiaij.fixedNORM.caching \
	     sharecpuGcc-60-PETSc-ICC-CG.mpiaij.fixedNORM \
	     cpuGcc-120-FOAM-DIC-PCG.fixedNORM \
	     cpuGcc-120-FOAM-GAMG-PCG.fixedNORM \
	     cpuGcc-120-PETSc-AMG-CG.mpiaij.fixedNORM \
	     cpuGcc-120-PETSc-AMG-CG.mpiaij.fixedNORM.caching \
	     cpuGcc-120-PETSc-ICC-CG.mpiaij.fixedNORM \
	     shareGcc-30-1-PCG_CLASSICAL_V_JACOBI \
	     shareGcc-60-2-PCG_CLASSICAL_V_JACOBI \
	     Gcc-120-4-PCG_CLASSICAL_V_JACOBI \
	     Gcc-120-8-PCG_CLASSICAL_V_JACOBI \
	     sharecpuGccN-30-FOAM-DIC-PCG.fixedNORM \
	     sharecpuGccN-30-FOAM-GAMG-PCG.fixedNORM \
	     sharecpuGccN-30-PETSc-AMG-CG.mpiaij.fixedNORM \
	     sharecpuGccN-30-PETSc-AMG-CG.mpiaij.fixedNORM.caching \
	     sharecpuGccN-30-PETSc-ICC-CG.mpiaij.fixedNORM \
	     sharecpuGccN-60-FOAM-DIC-PCG.fixedNORM \
	     sharecpuGccN-60-FOAM-GAMG-PCG.fixedNORM \
	     sharecpuGccN-60-PETSc-AMG-CG.mpiaij.fixedNORM \
	     sharecpuGccN-60-PETSc-AMG-CG.mpiaij.fixedNORM.caching \
	     sharecpuGccN-60-PETSc-ICC-CG.mpiaij.fixedNORM \
	     cpuGccN-120-FOAM-DIC-PCG.fixedNORM \
	     cpuGccN-120-FOAM-GAMG-PCG.fixedNORM \
	     cpuGccN-120-PETSc-AMG-CG.mpiaij.fixedNORM \
	     cpuGccN-120-PETSc-AMG-CG.mpiaij.fixedNORM.caching \
	     cpuGccN-120-PETSc-ICC-CG.mpiaij.fixedNORM \
	     shareGccN-30-1-PCG_CLASSICAL_V_JACOBI \
	     shareGccN-60-2-PCG_CLASSICAL_V_JACOBI \
	     GccN-120-4-PCG_CLASSICAL_V_JACOBI \
	     GccN-120-8-PCG_CLASSICAL_V_JACOBI \
	     sharempsGcc-30-1-PCG_CLASSICAL_V_JACOBI \
	     sharempsGcc-60-2-PCG_CLASSICAL_V_JACOBI \
	     mpsGcc-120-4-PCG_CLASSICAL_V_JACOBI \
	     sharempsGccN-30-1-PCG_CLASSICAL_V_JACOBI \
	     sharempsGccN-60-2-PCG_CLASSICAL_V_JACOBI \
	     mpsGccN-120-4-PCG_CLASSICAL_V_JACOBI \
	
     do
	 if [[ "${Case}" =~ -8- ]]
	 then
	     match="112"
	 elif  [[ "${Case}" =~ ^share ]]
	 then
	     match="\(30\|60\|90\)"
	 else
	     match="\(120\|240\|480\|960\|1920\|3840\|7680\|15360\)"
	 fi
	 grep ",${match},${Case}," time.csv | awk -F ',' '{node[$2]=$1;t[$2]+=$10;n[$2]++;t2sum[$2]+=$10**2} END {for (i in t) {mean=t[i]/n[i];sd=sqrt((t2sum[i]/n[i]-mean**2));print(i,node[i],mean,sd,n[i])}}' > time/${Case}
	 [ ! -s time/${Case} ] && rm time/${Case}
     done
     gnuplot plot.gp
    )
done
