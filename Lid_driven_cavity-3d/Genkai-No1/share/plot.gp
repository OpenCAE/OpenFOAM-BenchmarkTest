set terminal pdfcairo solid color lw 2 font "Helvetica,14"
set style data linespoints
set logscale xy
set format y "10^{%T}"
set grid
set pointsize 0.75
set xlabel "Number of nodes" offset 0,-0.5
set xrange [xmin:xmax]

set output "node-cost.pdf"
set key top left font "Courier-Bold,10" maxrows 4
set yrange [*:*]
set ylabel "Cost per step [yen] (<Lower is better)" 
plot \
 "< ls time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI | grep -v mps | xargs sort -n" using ($1/120):($3/3600*$1/120*120) title "GPU,AmgX-CG-AMG"\
,"< ls time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI | grep mps | xargs sort -n" using ($1/120):($3/3600*$1/120*120) title "GPU,AmgX-CG-AMG,MPS"\
,"< ls time/*Gcc-*-*-PCG_CLASSICAL_V_JACOBI | grep -v mps | xargs sort -n" using ($1/120):($3/3600*$1/120*120) title "GPU,AmgX-CG-AMG,DEVICES"\
,"< sort -n time/*cpuGccN-*-FOAM-DIC-PCG.fixedNORM" using ($1/120):($3/3600*$1/120*30) title "CPU,PCG-DIC"\
,"< sort -n time/*cpuGccN-*-FOAM-GAMG-PCG.fixedNORM" using ($1/120):($3/3600*$1/120*30) title "CPU,PCG-GAMG"\
,"< sort -n time/*cpuGccN-*-PETSc-ICC-CG.mpiaij.fixedNORM" using ($1/120):($3/3600*$1/120*30) title "CPU,PETSc-CG-ICC"\
,"< sort -n time/*cpuGccN-*-PETSc-AMG-CG.mpiaij.fixedNORM" using ($1/120):($3/3600*$1/120*30) title "CPU,PETSc-CG-AMG"\
,"< sort -n time/*cpuGccN-*-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using ($1/120):($3/3600*$1/120*30) title "CPU,caching"

set output "node-time.pdf"
set key top right font "Courier-Bold,10" maxrows 4
set yrange [*:timemax]
set ylabel "Exectution time per step [h] (<Lower is better)" 
plot \
 "< ls time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI | grep -v mps | xargs sort -n " using ($1/120):($3/3600) title "GPU,AmgX-CG-AMG"\
,"< ls time/*GccN-*-*-PCG_CLASSICAL_V_JACOBI | grep mps | xargs sort -n " using ($1/120):($3/3600) title "GPU,AmgX-CG-AMG,MPS"\
,"< ls time/*Gcc-*-*-PCG_CLASSICAL_V_JACOBI | grep -v mps | xargs sort -n" using ($1/120):($3/3600) title "GPU,AmgX-CG-AMG,DEVICES"\
,"< sort -n time/*cpuGccN-*-FOAM-DIC-PCG.fixedNORM" using ($1/120):($3/3600) title "CPU,PCG-DIC"\
,"< sort -n time/*cpuGccN-*-FOAM-GAMG-PCG.fixedNORM" using ($1/120):($3/3600) title "CPU,PCG-GAMG"\
,"< sort -n time/*cpuGccN-*-PETSc-ICC-CG.mpiaij.fixedNORM" using ($1/120):($3/3600) title "CPU,PETSc-CG-ICC"\
,"< sort -n time/*cpuGccN-*-PETSc-AMG-CG.mpiaij.fixedNORM" using ($1/120):($3/3600) title "CPU,PETSc-CG-AMG"\
,"< sort -n time/*cpuGccN-*-PETSc-AMG-CG.mpiaij.fixedNORM.caching" using ($1/120):($3/3600) title "CPU,caching"\
,timemax/(x/xmin) title "" with l lt 1 lc 0 lw 0.5
