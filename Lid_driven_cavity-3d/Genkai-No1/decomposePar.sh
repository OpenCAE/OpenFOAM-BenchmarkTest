#!/bin/sh
#PJM -L rg=a-batch
#PJM -L node=1
#PJM -L elapse=24:00:00
#PJM -j
#PJM -S
prefix=${HOME}/opt/local;openfoam_version=v2406;gcc_version=12;ompi_version=4.1.6;module purge;module load gcc-toolset/${gcc_version} ompi/${ompi_version};machine=$(uname -m);dir=${prefix}/${machine}/apps/gcc/${gcc_version}/ompi/${ompi_version};openfoam_dir=${dir}/openfoam/${openfoam_version};source ${openfoam_dir}/OpenFOAM-${openfoam_version}/etc/bashrc;export OMP_NUM_THREADS=1;export FOAM_SIGFPE=false
decomposePar &> log.decomposePar
