timemax=1e-0
xmin=1
xmax=128
set xrange [xmin:xmax]
set xtics ("1/4\n(1GPU)" 0.25,"1/2\n(2GPU)" 0.5,"1\n(4GPU)" 1,"2\n(8GPU)" 2,"4\n(16GPU)" 4,8,16,32,64,128)  font "Helvetica,12"
load '../share/plot.gp'
