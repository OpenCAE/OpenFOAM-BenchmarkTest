# Oracle Cloud Infrastructure AmpereAltra (test, pre-service) 1socket

## General Iinformation
* Measurer: Satoshi Ohshima
* Date: April, 2021

## Hardware
* Oracle Cloud Infrastructure AmpereAltra instance
  * CPU: Ampere Altra Q80-30 (80cores, 3.0GHz)

## Compiler
* GCC 9.3.1 (devtoolset)

### Optimize Options
- -O0
- -O3
- -Ofast
- -O3 -march=native
- -Ofast -march=native
- -O3 -march=neoverse
- -Ofast -march=neoverse
- -O3 -march=armv8.2-a
- -Ofast -march=armv8.2-a

## MPI
* OpenMPI 3.1.3
