#!/bin/bash
#
# Main
#
MAX_NUMBER_OF_QUEUE=128
# 最適化スクリプトで生成されたパラメータ定義ファイルを評価
source $0.param
# ppn(process per node)とノード数から，領域分割数を算出
numberOfSubdomains=`expr $decomposeParDict_ppn \* $decomposeParDict_node`
# 元のケースディレクトリから必要なディレクトリをリンク
if [ ! -d include ]
then
    mkdir include
    (cd include
	ln -s  ../../include/* ./
    )
fi
for file in 0 constant system
do
    [ -L $file ] || ln -s ../$file .
done
# decomposeParDictを生成
rm -f include/decomposeParDict
cat > include/decomposeParDict <<EOF
numberOfSubdomains $numberOfSubdomains; // 領域分割数(並列数)
method $decomposeParDict_method; // 分割手法
preservePatches (sides_half0 sides_half1 inout_half0 inout_half1);
multiLevelCoeffs
{
  method  scotch;
  domains ($decomposeParDict_ppn $decomposeParDict_node);
}
EOF
cat > decomposePar.sh <<EOF
#!/bin/bash
#PJM -g gt00
#PJM -L rscgrp=lecture
#PJM -L elapse=0:15:00
#PJM -L node=1
#PJM --mpi proc=1
#PJM -S
#PJM -j
module purge
module load gcc/4.8.5
module load openfoam/v2012
source \$WM_PROJECT_DIR/etc/bashrc
decomposePar >& log.$0.\$PJM_JOBID
EOF
chmod +x decomposePar.sh
while :
do
    nq=`pjstat | grep "^[0-9]" | wc -l`
    [ "$nq" -lt "$MAX_NUMBER_OF_QUEUE" ] &&  break
    sleep 5
done
JOBID=$(pjsub -z jid decomposePar.sh)
echo $JOBID
exit 0
