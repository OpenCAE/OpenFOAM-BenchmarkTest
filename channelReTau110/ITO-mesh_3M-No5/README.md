# ITO-mesh_3M-No5

## General information

* Measurer: Satoshi Oshima, Masashi Imano
* Date: Dec 2018

## Benchmark condition

* Number of mesh: 2995200(3M)
* Number of nodes: 1-32
* Number of processors per node: 36
* Number of trial: 1

## Other information

See https://gitlab.com/OpenCAE/OpenFOAM-BenchmarkTest/blob/master/channelReTau110/ITO-mesh_any-No5/README.md
