# ITO-mesh_24M-No7

## General information

* Measurer: Masashi Imano
* Date: July 2019

## Benchmark condition

* Number of cells: 23961600(24M)
* Number of nodes: 1-8
* Number of processors per node: 36
* Number of trial: 5

## Other information

See https://gitlab.com/OpenCAE/OpenFOAM-BenchmarkTest/blob/master/channelReTau110/ITO-mesh_any-No7/README.md
