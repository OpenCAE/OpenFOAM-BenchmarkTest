#!/bin/sh

source ./case.sh

figs=""
for case in ${caseArray[@]}
do
    echo $case
    tmpfigs=`python ../bin/plot.py $case.csv --sph --ExecutionTimePerStep ExecutionTimePerStep;python ../bin/plot.py $case.csv --pe -y --ExecutionTimePerStep ExecutionTimePerStep`
    echo $tmpfigs
    figs="$figs $tmpfigs"
    echo
done

base=`basename ${PWD}`
CONVERT=$(which convert 2> /dev/null)
if [ ! "x$CONVERT" = "x" ]
then
    $CONVERT -density 300 ${figs} ${base}.pdf
    rm -f ${figs}
else
    echo "convert not installed" >&2
fi
