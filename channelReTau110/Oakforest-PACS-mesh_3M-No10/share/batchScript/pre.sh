#!/bin/bash

if [ "$PJM_ENVIRONMENT" != "INTERACT" ];then
    if [ -z "${PJM_JOBID+x}" ] ; then
	PJM_JOBID=$1
    else
	export HOME=/work/$(id -gn)/$(id -un)
	cd $PJM_O_WORKDIR
    fi
fi

module purge
module load gcc/4.8.5
module unload impi
module load impi/2017.1.132
module load openfoam/4.1

export MPI_ROOT=$I_MPI_ROOT

source_command="source $WM_PROJECT_DIR/etc/bashrc"
$source_command

# Application name
application=pre.sh

# Log file
log=log.${application}.${PJM_JOBID}
batchFileDone=${application}.done

echo "source_command:" > $log 2>&1
echo $source_command >> $log 2>&1
echo "evv:" >> $log 2>&1
env >> $log 2>&1
exec_command="blockMesh"
echo "exec_command:" >> $log 2>&1
echo $exec_command >> $log 2>&1
$exec_command >> $log 2>&1
grep "^End" $log >& /dev/null && touch $batchFileDone

#------------------------------------------------------------------------------
