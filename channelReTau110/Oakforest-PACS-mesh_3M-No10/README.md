# Oakforest-PACS-mesh_3M-No10

## General information

* Measurer:  Masashi Imano
* Date: Feb 20 2019(JST)

## Benchmark condition

* Number of mesh：2995200(3M)
* Number of processors(Number of node): 64(1) - 2048(32)

## Hardware

* Name: Oakforest-PACS (http://www.cc.u-tokyo.ac.jp/system/ofp/)
* Site: Joint Center for Advanced High Performance Computing (http://jcahpc.jp/eng/index.html)
* System
  * Number of Node: 8208
  * Peak performance: 25.004 PFlops
  * Memory: 897 TByte
  * Network topology: Full-bisection Fat Tree
* Node
  * Name: Fujitsu PRIMERGY CX1640 M1
  * CPU
    * Processor: Intel® Xeon Phi 7250 (Knights Landing)
    * Number of Processor(core): 1(68)
    * Frequency: 1.4GHz
    * Peak performance: 3.0464 TFlops
  * Memory
    * Size: 96 GB(DDR4)＋ 16 GB(MCDRAM)
  * Interconnect: Intel Omni-Path (100Gbps)

## Solve batch script 

Solve batch script name(legend of plot):

nPPN-RAM_MODE-COMPILER-MPLIB-OPTIONS-MPI_BUFFER_SIZE-HBM_THRESHOLD-HBM_SIZE

### PPN

Number of processes per node.

* 64

### RAM_MODE

Ram mode.

* f: flat mode 

### COMPILER

* Icc2019_1_144KNL: Intel Compiler 2019.1.144 with KNL option

### MPLIB

* INTELMPI2019_1_144: Intel MPI 2019.1.144

### OPTIONS

* h: use libhbm (https://github.com/OpenFOAM/OpenFOAM-Intel/tree/master/libhbm)

## How to plot results

Execute Allrun.plot

```bash
./Allrun.plot
```

## Note
