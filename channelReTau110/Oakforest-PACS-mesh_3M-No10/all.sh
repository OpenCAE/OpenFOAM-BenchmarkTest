MAX_NUMBER_OF_LOOP=5

BATCH_PRE=0
BATCH_DECOMPOSEPAR=0
BATCH_SOLVE=1

MAX_NUMBER_OF_QUEUE=160

fvSolutionArray=(
PCG-DIC
)

decomposeParDictArray=(
mpi_00064-method_scotch
mpi_00128-method_scotch
mpi_00256-method_scotch
mpi_00512-method_scotch
mpi_01024-method_scotch
mpi_02048-method_scotch
)

solveBatchArray=(
n64-f-Icc2019_1_144KNL-INTELMPI2019_1_144-h-6291456-4-240
)

makeCases()
{
    local mx=240
    local my=130
    local mz=96
    local deltaT=0.002
    local endTime=0.102

    mkdir cases
    cd cases
     cp -a ../../template/* ./

    sed -i \
    -e s/"^ *mx .*"/"mx $mx;"/ \
    -e s/"^ *my .*"/"my $my;"/ \
    -e s/"^ *mz .*"/"mz $mz;"/ \
    system/blockMeshDict

    cp -a system/blockMeshDict constant/polyMesh/

    sed -i \
    -e s/"^\( *deltaT \).*"/"\1 $deltaT;"/ \
    -e s/"^\( *endTime \).*"/"\1 $endTime;"/ \
    -e s/"^\( *writeFormat \).*"/"\1 ascii;"/ \
    system/controlDict

    cd ../
}

NumberOfBatchQueue()
{
    pjstat -l | grep "^[0-9]" | wc -l
}

BatchSubmit()
{
    local BATCHFILE=$1
    local MPI=$2

    echo "BATCHFILE: $BATCHFILE" 
    echo "MPI: $MPI" 

    local GROUP=$(id -gn)
    local NPROCS_PER_NODE=64
    local PPN=64
    local RAM_MODE=cache
    local QUEUE=regular-$RAM_MODE
    local WALLTIME=00:30:00

    if [ "$BATCHFILE" = "decomposePar.sh" ];then
	MPI=1
	WALLTIME=04:00:00
    else
	local CASE_NAME=${BATCHFILE}
	PPN=${CASE_NAME%%-*};CASE_NAME=${CASE_NAME#*-}
	PPN=${PPN#n}
	RAM_MODE_TMP=${CASE_NAME%%-*};CASE_NAME=${CASE_NAME#*-}
	if [ "$RAM_MODE_TMP" = "c" -o "$RAM_MODE_TMP" = "C" ]
	then
	    RAM_MODE=cache
	else
	    RAM_MODE=flat
	fi
	QUEUE=debug-$RAM_MODE
    fi

    echo "PPN: $PPN" 
    echo "QUEUE: $QUEUE"
	    
    local NODE=`echo "($MPI + $PPN - 1)/ $PPN" | bc`
    echo "NODE: $NODE" 

    if [ "$BATCHFILE" != "pre.sh" -a "$BATCHFILE" != "decomposePar.sh" ];then
	local MPI_TMP=`echo "$NODE * $PPN" | bc`
	echo "NODE * PPN: $MPI_TMP"
        if [ $MPI -ne $MPI_TMP ];then
	    echo "Number of MPI is mismatch. Skip running."
  	    return 0
	fi  
    fi

    if [ $NODE -le 16 ]
    then
	GROUP=gt00
	QUEUE=lecture-$RAM_MODE
	WALLTIME=00:15:00
    fi

    local COMMAND="pjsub -S -g ${GROUP} -L rscgrp=${QUEUE},node=${NODE},elapse=${WALLTIME} --mpi proc=${MPI} ${BATCHFILE}"
    echo "Executing $COMMAND"
    local MSG=`$COMMAND`
    echo $MSG
    local JOBID=`echo $MSG | awk '{if ($7=="submitted.") {print $6} else {print "fail"} }'`
    echo $JOBID
}
