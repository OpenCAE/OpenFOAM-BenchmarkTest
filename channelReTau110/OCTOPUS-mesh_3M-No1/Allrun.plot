#!/bin/sh

[ -d nodetime ] || mkdir nodetime

maxNumberOfSampling=1

figs=""
for batch in \
    n12-v1612+-system-Icc18_0_2 \
    n16-v1612+-system-Icc18_0_2 \
    n20-v1612+-system-Icc18_0_2 \
    n24-v1612+-system-Icc18_0_2 \
    n32-v1612+-system-Icc18_0_2 \
    n64-v1612+-system-Icc18_0_2
do
    echo $batch
    tempfile=$(mktemp)
    head -n 1 all.csv > $tempfile
    grep ",PCG-DIC,$batch" all.csv >> $tempfile
    batchFileName=$(cut -d ',' -f 4 $tempfile | sort | uniq | grep $batch)
    fig=`python ../bin/plot.py $tempfile \
	-m $maxNumberOfSampling \
	--sph \
	--ExecutionTimePerStep ExecutionTimePerStep \
	-b $batchFileName;\
        python ../bin/plot.py $tempfile \
	-m $maxNumberOfSampling \
	--pe -y \
	--ExecutionTimePerStep ExecutionTimePerStep \
	-b $batchFileName`
    figs="$figs $fig"
done

base=`basename ${PWD}`
CONVERT=$(which convert 2> /dev/null)
if [ ! "x$CONVERT" = "x" ]
then
    $CONVERT -density 300 ${figs} ${base}.pdf
    GS=$(which gs 2> /dev/null)
    if [ ! "x$GS" = "x" ]
    then
	gs \
-dNOPAUSE \
-dBATCH \
-dQUIET \
-sDEVICE=pdfwrite \
-dCompatibilityLevel=1.4 \
-dAutoFilterGrayImages=true \
-dGrayImageFilter=/DCTEncode \
-dEncodeGrayImages=true \
-dDownsampleGrayImages=true \
-dGrayImageDownsampleThreshold=1.5 \
-dGrayImageDownsampleType=/Bicubic \
-dGrayImageResolution=150 \
-dMonoImageFilter=/CCITTFaxEncode \
-dEncodeMonoImages=true \
-dDownsampleMonoImages=true \
-dMonoImageDownsampleThreshold=1.5 \
-dMonoImageDownsampleType=/Bicubic \
-dMonoImageResolution=300 \
-dAutoFilterColorImages=true \
-dColorImageFilter=/DCTEncode \
-dEncodeColorImages=true \
-dColorImageResolution=150 \
-dColorImageDownsampleThreshold=1.5 \
-dColorImageDownsampleType=/Bicubic \
-sOutputFile=${base}-compressed.pdf \
	${base}.pdf
	rm -f ${base}.pdf
    else
	echo "gs not installed" >&2
    fi
else
    echo "convert not installed" >&2
fi
