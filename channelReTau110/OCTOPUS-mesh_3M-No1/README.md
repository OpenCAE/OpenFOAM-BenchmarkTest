# OCTOPUS-mesh_3M-No1

## General information

* Measurer: Masashi Imano
* Date: Jul 2018 - Aug 2018

## Benchmark condition

* Number of mesh：2995200(3M)

## Hardware

* Name: OCTOPUS (www.hpc.cmc.osaka-u.ac.jp/octopus/)

## Solve batch script 

Solve batch script name(legend of plot):

nPPN-FOAM_VERSION-COMPILER_TYPE-COMPILER-COMPILE_OPTION-MPLIB-OPTIONS

### PPN

Number of processes per node.

* 12 (OCTOPUS)
* 16 (OCTOPUS)
* 20 (OCTOPUS)
* 24 (OCTOPUS)
* 32 (OCTPHI)
* 64 (OCTPHI)

### FOAM_VERSION

OpenFOAM version.

* v1612+: OpenFOAM-v1612+

### COMPILER_TYPE

* system: System compiler

### COMPILER

* Icc18_0_2: Intel Compiler 2018 Update 2

### COMPILE_OPTION

* Opt: -O3 (default)

### MPLIB

* INTELMPI2018_2: Intel MPI Update 2

### OPTIONS

* Additional options: 
  * 6: export I_MPI_DEBUG=6
  * D: export I_MPI_DYNAMIC_CONNECTION=0
  * U: export I_MPI_DAPL_UD_TRANSLATION_CACHE=0
  * p: numactl --preferred=1

## How to plot results

Execute Allrun.plot

```bash
./Allrun.plot
```

## Note
