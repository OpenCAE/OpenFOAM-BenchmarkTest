#!/bin/bash

if [ -z "${PBS_JOBID+x}" ] ; then
    PBS_JOBID=$1
else
    cd $PBS_O_WORKDIR
fi

source /octfs/apl/OpenFOAM/4.1/OpenFOAM-4.1/etc/bashrc

application=pre.sh

log=log.${application}.${PBS_JOBID}
(
    env
    blockMesh
) >& $log

grep "^End" $log >& /dev/null && touch $application.done

#------------------------------------------------------------------------------
