MAX_NUMBER_OF_LOOP=1

BATCH_PRE=0
BATCH_DECOMPOSEPAR=1
BATCH_SOLVE=1

MAX_NUMBER_OF_QUEUE=400

fvSolutionArray=(
PCG-DIC
PCG-GAMG-symGaussSeidel
PCG-GAMG-symGaussSeidelKNL
)

GPU_OPTION=""
case "$configuration" in
    n1)
	QUEUE="OCTOPUS"
	GPU_OPTION="-l gpunum_job=1"
        decomposeParDictArray=(
	    mpi_00001-method_scotch
	)

	solveBatchArray=(
	    n1-dev-system-Nvcc9_0-Opt-SYSTEMOPENMPI3_0_0rc6-
	)
	;;
    n4)
	QUEUE="OCTOPUS"
	GPU_OPTION="-l gpunum_job=4"
        decomposeParDictArray=(
	    mpi_00004-method_scotch
	)

	solveBatchArray=(
	    n4-dev-system-Nvcc9_0-Opt-SYSTEMOPENMPI3_0_0rc6-
	)
	;;
    n12)
	QUEUE="OCTOPUS"
        decomposeParDictArray=(
	    mpi_00012-method_scotch
	    mpi_00024-method_scotch
	    mpi_00048-method_scotch
	    mpi_00096-method_scotch
	    mpi_00192-method_scotch
	    mpi_00384-method_scotch
	    mpi_00768-method_scotch
	    mpi_01536-method_scotch
	)

	solveBatchArray=(
	    n12-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTU
	    n12-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTUV
	    n12-v1612+V-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTU
	    n12-v1612+V-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTUV
	)
	;;
    n16)
	QUEUE="OCTOPUS"
        decomposeParDictArray=(
	    mpi_00016-method_scotch
	    mpi_00032-method_scotch
	    mpi_00064-method_scotch
	    mpi_00128-method_scotch
	    mpi_00256-method_scotch
	    mpi_00512-method_scotch
	    mpi_01024-method_scotch
	    mpi_02048-method_scotch
	)

	solveBatchArray=(
	    n16-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTU
	    n16-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTUV
	    n16-v1612+V-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTU
	    n16-v1612+V-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTUV
	)
	;;
    n20)
	QUEUE="OCTOPUS"
        decomposeParDictArray=(
	    mpi_00020-method_scotch
	    mpi_00040-method_scotch
	    mpi_00080-method_scotch
	    mpi_00160-method_scotch
	    mpi_00320-method_scotch
	    mpi_00640-method_scotch
	    mpi_01280-method_scotch
	    mpi_02560-method_scotch
	)

	solveBatchArray=(
	    n20-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTU
	    n20-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTUV
	    n20-v1612+V-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTU
	    n20-v1612+V-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTUV
	)
	;;
    n24)
	QUEUE="OCTOPUS"
        decomposeParDictArray=(
	    mpi_00024-method_scotch
	    mpi_00048-method_scotch
	    mpi_00096-method_scotch
	    mpi_00192-method_scotch
	    mpi_00384-method_scotch
	    mpi_00768-method_scotch
	    mpi_01536-method_scotch
	    mpi_03072-method_scotch
	)

	solveBatchArray=(
	    n24-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTU
	    n24-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTUV
	    n24-v1612+V-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTU
	    n24-v1612+V-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTUV
	)
	;;
    n32)
	QUEUE="OCTPHI"
        decomposeParDictArray=(
	    mpi_00032-method_scotch
	    mpi_00064-method_scotch
	    mpi_00128-method_scotch
	    mpi_00256-method_scotch
	    mpi_00512-method_scotch
	    mpi_01024-method_scotch
	)

	solveBatchArray=(
	    n32-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DpTU
	    n32-v1612+-system-Icc18_0_2KNL-Opt-INTELMPI2018_2-6DpTU
	    n32-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTU
	    n32-v1612+-system-Icc18_0_2KNL-Opt-INTELMPI2018_2-6DTU
	    n32-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DpTUV
	    n32-v1612+-system-Icc18_0_2KNL-Opt-INTELMPI2018_2-6DpTUV
	    n32-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTUV
	    n32-v1612+-system-Icc18_0_2KNL-Opt-INTELMPI2018_2-6DTUV
	    n32-v1612+V-system-Icc18_0_2KNL-Opt-INTELMPI2018_2-6DpTU
	    n32-v1612+V-system-Icc18_0_2KNL-Opt-INTELMPI2018_2-6DpTUV
	)
	;;
    n64)
	QUEUE="OCTPHI"
        decomposeParDictArray=(
	    mpi_00064-method_scotch
	    mpi_00128-method_scotch
	    mpi_00256-method_scotch
	    mpi_00512-method_scotch
	    mpi_01024-method_scotch
	    mpi_02048-method_scotch
	)

	solveBatchArray=(
	    n64-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DpTU
	    n64-v1612+-system-Icc18_0_2KNL-Opt-INTELMPI2018_2-6DpTU
	    n64-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTU
	    n64-v1612+-system-Icc18_0_2KNL-Opt-INTELMPI2018_2-6DTU
	    n64-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DpTUV
	    n64-v1612+-system-Icc18_0_2KNL-Opt-INTELMPI2018_2-6DpTUV
	    n64-v1612+-system-Icc18_0_2-Opt-INTELMPI2018_2-6DTUV
	    n64-v1612+-system-Icc18_0_2KNL-Opt-INTELMPI2018_2-6DTUV
	    n64-v1612+V-system-Icc18_0_2KNL-Opt-INTELMPI2018_2-6DpTU
	    n64-v1612+V-system-Icc18_0_2KNL-Opt-INTELMPI2018_2-6DpTUV
	)
	;;
esac

NumberOfBatchQueue()
{
    qstat -l | grep "^[0-9]" | wc -l
}

BatchSubmit()
{
    local BATCHFILE=$1
    local MPI=$2

    echo "BATCHFILE: $BATCHFILE"
    echo "MPI: $MPI"

    if [ "$BATCHFILE" = "pre.sh" -o "$BATCHFILE" = "decomposePar.sh" ];then
        local NPROCS_PER_NODE=1
	MPI=1
    else
        local NPROCS_PER_NODE=`echo $BATCHFILE | cut  -d "-"  -f 1 | tr -d 'n'`
    fi

    echo "NPROCS_PER_NODE: $NPROCS_PER_NODE"

    local NODE=`echo "($MPI + $NPROCS_PER_NODE - 1)/ $NPROCS_PER_NODE" | bc`

    echo "NODE: $NODE"

    if [ "$BATCHFILE" != "pre.sh" -a "$BATCHFILE" != "decomposePar.sh" ];then
	local MPI_TMP=`echo "$NODE * $NPROCS_PER_NODE" | bc`
	echo "NODE * NPROCS_PER_NODE: $MPI_TMP"
        if [ $MPI -ne $MPI_TMP ];then
	    echo "Number of MPI is mismatch. Skip running."
  	    return 0
	fi
	case $NODE in
	    1)
		WALLTIME="00:15:00"
		;;
	    2)
		WALLTIME="00:08:00"
		;;
	    *)
		WALLTIME="00:04:00"
		;;
	esac
    else	
	QUEUE="OCTOPUS"
	WALLTIME="02:00:00"
    fi

    Toption=""
    case $BATCHFILE in
	*INTELMPI*)
	    Toption="-T intmpi"
	    ;;
	*OPENMPI*)
	    Toption="-T openmpi"
	    ;;
    esac

    command="qsub \
	-q $QUEUE \
	-l elapstim_req=$WALLTIME \
	-l cpunum_job=$NPROCS_PER_NODE \
	-b $NODE \
	$Toption \
        $GPU_OPTION \
	$BATCHFILE"
    echo $command
    $command
}
