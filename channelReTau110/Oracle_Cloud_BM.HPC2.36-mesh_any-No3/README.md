# Oracle_Cloud_BM.HPC2.36-mesh_any-No3

## Hardware

* Name: Oracle Cloud Infrastructure BM.HPC2.36 (https://cloud.oracle.com/compute/bare-metal/features)

## Solve batch script 

Solve batch script name(legend of plot):

nPPN-FOAM_VERSION-COMPILER_TYPE-COMPILER-COMPILE_OPTION-MPLIB-OPTIONS

### PPN

Number of processes per node.

* 32
* 36

### FOAM_VERSION

OpenFOAM version.

* 2.3.0: OpenFOAM-2.3.0

### COMPILER_TYPE

* system: System compiler

### COMPILER

* Gcc4_8_5: Gcc 4.8.5

### COMPILE_OPTION

* Opt: -O3 (default)

### MPLIB

* SYSTEMOPENMPI3_1_0rc2: OpenMPI-3.1.0rc2
* SYSTEMOPENMPI3_1_1: OpenMPI-3.1.1
* INTELMPI5_1_1_109: Intel MPI 5.1.1.109
* INTELMPI2018_1_163: Intel MPI 2018.1.163

### OPTIONS

#### mpirun options for Intel MPI

* Default options:
```
-machinefile $PBS_NODEFILE \
-genv I_MPI_DAT_LIBRARY=/usr/lib64/libdat2.so \
-genv I_MPI_DAPL_PROVIDER=ofa-v2-cma-roe-enp94s0f0 \
-genv I_MPI_FALLBACK=0 \
-genv I_MPI_DEBUG=6 \
-genv I_MPI_PERHOST=$PPN \
```

* Additional options: 
  * l:
    * PPN=36: -genv I_MPI_PIN_PROCESSOR_LIST=0-17,18-35 
    * PPN=32: -genv I_MPI_PIN_PROCESSOR_LIST=0-15,18-31
  * L:
    * PPN=36: -genv I_MPI_PIN_PROCESSOR_LIST=18-35,0-17
    * PPN=32: -genv I_MPI_PIN_PROCESSOR_LIST=18-31,0-15
  * T: -genv I_MPI_DAPL_TRANSLATION_CACHE=0
  * U: -genv I_MPI_DAPL_UD_TRANSLATION_CACHE=0
  * p: -genv I_MPI_FABRICS=shm:tcp

#### mpirun options for OpenMPI

* Default options:
```
--report-bindings \
--display-allocation \
--display-map \
-machinefile $PBS_NODEFILE
```

* Additional options: 
  * 0: -mca btl self
  * 3: -x UCX_IB_GID_INDEX=3
  * 5: -x UCX_IB_TRAFFIC_CLASS=105
  * 7: -x UCX_TLS=rc,self,sm
  * h: -x HCOLL_ENABLE_MCAST_ALL=0
  * p: -mca btl vader,tcp,self
  * s:
    *PPN=36: --map-by ppr:18:socket 
    *PPN=32: --map-by ppr:16:socket 

## How to plot results

Link settings and shell scripts if Allrun.plot does not exists.

```bash
../bin/linkSettings.sh Oracle_Cloud_BM.HPC2.36-mesh_any-No3
```

Execute Allrun.plot

```bash
./Allrun.plot
```

## Note

Hyper threading is disabled.
