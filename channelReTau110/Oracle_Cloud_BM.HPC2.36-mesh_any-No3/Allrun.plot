#!/bin/sh

[ -d nodetime ] || mkdir nodetime
batch=all
python ../bin/nodetime.py $batch.csv > nodetime/$batch.csv

maxNumberOfSampling=5

figs=""
for batch in \
n36-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1 \
n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1 \
n36-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_0rc2 \
n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_0rc2 \
n36-2.3.0-system-Gcc4_8_5-Opt-INTELMPI5_1_1_109 \
n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI5_1_1_109 \
n36-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2018_1_163 \
n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2018_1_163
do
    echo $batch
    tempfile=$(mktemp)
    head -n 1 all.csv > $tempfile
    grep $batch all.csv >> $tempfile
    python ../bin/nodetime.py $tempfile > nodetime/$batch.csv
    batchFileName=$(cut -d ',' -f 4 $tempfile | sort | uniq | grep $batch)
    fig=`python ../bin/plot.py $tempfile \
	-m $maxNumberOfSampling \
	--sph \
	--ExecutionTimePerStep ExecutionTimePerStep \
	--legendFontSize 5 \
	--tickFontSize 5 \
	-b $batchFileName`
    mv $fig $batch.pdf
    figs="$figs $batch.pdf"
done

base=`basename $PWD`
CONVERT=$(which convert 2> /dev/null)
if [ ! "x$CONVERT" = "x" ]
then
    $CONVERT -density 300 $figs $base.pdf
    rm -f $figs 
else
    echo "convert not installed" >&2
fi
