WORKDIR=${PWD##*/}

case ${WORKDIR} in
    *-mesh_0.37M-*)
	MAX_NUMBER_OF_LOOP=5
	;;
    *-mesh_3M-*)
	MAX_NUMBER_OF_LOOP=5
	;;
    *-mesh_24M-*)
	MAX_NUMBER_OF_LOOP=1
	;;
esac


BATCH_PRE=0
BATCH_DECOMPOSEPAR=0
BATCH_SOLVE=1

MAX_NUMBER_OF_QUEUE=5000

fvSolutionArray=(
PCG-DIC
)

case "$configuration" in
    n32)
        decomposeParDictArray=(
mpi_00032-method_scotch
mpi_00064-method_scotch
mpi_00096-method_scotch
mpi_00128-method_scotch
mpi_00160-method_scotch
mpi_00192-method_scotch
mpi_00224-method_scotch
mpi_00256-method_scotch
mpi_00384-method_scotch
mpi_00512-method_scotch
mpi_00640-method_scotch
mpi_00768-method_scotch
mpi_00832-method_scotch
	)

	solveBatchArray=(
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-lTU
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-LTU
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-TU
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI5_1_1_109-lTU
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI5_1_1_109-LTU
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI5_1_1_109-TU
	    n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_0rc2-0357Hs
	    n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1-0357Hs
	)
	;;
    n32-tcp)
        decomposeParDictArray=(
mpi_00032-method_scotch
mpi_00064-method_scotch
mpi_00096-method_scotch
mpi_00128-method_scotch
mpi_00160-method_scotch
mpi_00192-method_scotch
mpi_00224-method_scotch
mpi_00256-method_scotch
mpi_00384-method_scotch
mpi_00512-method_scotch
mpi_00640-method_scotch
mpi_00768-method_scotch
mpi_00832-method_scotch
	)

	solveBatchArray=(
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-pTU
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI5_1_1_109-pTU
	    n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_0rc2-ps
	    n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1-ps
	)
	;;
    n36)
        decomposeParDictArray=(
mpi_00036-method_scotch
mpi_00072-method_scotch
mpi_00108-method_scotch
mpi_00144-method_scotch
mpi_00180-method_scotch
mpi_00216-method_scotch
mpi_00252-method_scotch
mpi_00288-method_scotch
mpi_00432-method_scotch
mpi_00576-method_scotch
mpi_00720-method_scotch
mpi_00864-method_scotch
mpi_00936-method_scotch
	)

	solveBatchArray=(
	    n36-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-LTU
	    n36-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-TU
	    n36-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-
	    n36-2.3.0-system-Gcc4_8_5-Opt-INTELMPI5_1_1_109-LTU
	    n36-2.3.0-system-Gcc4_8_5-Opt-INTELMPI5_1_1_109-TU
	    n36-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_0rc2-0357Hs
	    n36-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1-0357Hs
	)
	;;
    n36-tcp)
        decomposeParDictArray=(
mpi_00036-method_scotch
mpi_00072-method_scotch
mpi_00108-method_scotch
mpi_00144-method_scotch
mpi_00180-method_scotch
mpi_00216-method_scotch
mpi_00252-method_scotch
mpi_00288-method_scotch
mpi_00432-method_scotch
mpi_00576-method_scotch
mpi_00720-method_scotch
mpi_00864-method_scotch
mpi_00936-method_scotch
	)

	solveBatchArray=(
	    n36-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-pTU
	    n36-2.3.0-system-Gcc4_8_5-Opt-INTELMPI5_1_1_109-pTU
	    n36-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_0rc2-ps
	    n36-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1-ps
	)
	;;
esac

NumberOfBatchQueue()
{
	qstat | grep "^[0-9]" | wc -l
}

BatchSubmit()
{
    local BATCHFILE=$1
    local MPI=$2

    echo "BATCHFILE: $BATCHFILE"
    echo "MPI: $MPI"

    if [ "$BATCHFILE" = "pre.sh" -o "$BATCHFILE" = "decomposePar.sh" ];then
        local NPROCS_PER_NODE=1
	MPI=1
    else
        local NPROCS_PER_NODE=`echo $BATCHFILE | cut  -d "-"  -f 1 | tr -d 'n'`
    fi

    echo "NPROCS_PER_NODE: $NPROCS_PER_NODE"

    local NODE=`echo "($MPI + $NPROCS_PER_NODE - 1)/ $NPROCS_PER_NODE" | bc`

    echo "NODE: $NODE"

    if [ "$BATCHFILE" != "pre.sh" -a "$BATCHFILE" != "decomposePar.sh" ];then
	local MPI_TMP=`echo "$NODE * $NPROCS_PER_NODE" | bc`
	echo "NODE * NPROCS_PER_NODE: $MPI_TMP"
        if [ $MPI -ne $MPI_TMP ];then
	    echo "Number of MPI is mismatch. Skip running."
  	    return 0
	fi
	case ${WORKDIR} in
	    *-mesh_0.37M-*)
		WALLTIME="00:10:00"
		;;
	    *-mesh_3M-*)
		WALLTIME="00:20:00"
		;;
	    *-mesh_24M-*)
		case "$configuration" in
		    *-tcp)
			WALLTIME="02:00:00"
			;;
		    *)
			WALLTIME="00:30:00"
			;;
		esac
		;;
	esac
    else
	WALLTIME="02:00:00"
    fi

    echo ${WALLTIME}

    command="qsub \
        -j oe \
        -l walltime=${WALLTIME} \
        -l select=$NODE:ncpus=$NPROCS_PER_NODE:mpiprocs=$NPROCS_PER_NODE:ompthreads=1 \
	-l place=scatter:excl  \
        $BATCHFILE"
    echo $command
    $command
}
