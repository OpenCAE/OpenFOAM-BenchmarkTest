#!/bin/bash

. ~/OpenFOAM/installOpenFOAM/install.sh -s OpenFOAM_VERSION=OpenFOAM-2.3.0,COMPILER_TYPE=system,COMPILER=Gcc4_8_5,COMPILE_OPTION=Opt,ARCH_OPTION=64,PRECISION_OPTION=DP,LABEL_SIZE=32,MPLIB=INTELMPI2018_1_163

application=pre.sh

(
env
blockMesh
) >& log.${application}.$1

touch ${application}.done
