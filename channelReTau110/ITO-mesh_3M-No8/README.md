# ITO-mesh_3M-No8

## General information

* Measurer: Masashi Imano
* Date: July 2019

## Benchmark condition

* Number of mesh: 2995200(3M)
* Number of nodes: 1-8
* Number of processors per node: 36
* Number of trial: 1

## Other information

See https://gitlab.com/OpenCAE/OpenFOAM-BenchmarkTest/blob/master/channelReTau110/ITO-mesh_any-No8/README.md
