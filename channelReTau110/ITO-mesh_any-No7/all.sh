WORKDIR=${PWD##*/}

case ${WORKDIR} in
    *-mesh_0.37M*)
	WALLTIME="00:05:00"
	MAX_NUMBER_OF_LOOP=1
	;;
    *-mesh_3M*)
	WALLTIME="00:05:00"
	MAX_NUMBER_OF_LOOP=1
	;;
    *-mesh_24M*)
	WALLTIME="00:10:00"
	MAX_NUMBER_OF_LOOP=1
	;;
esac

case "$configuration" in
    n32)
	case ${WORKDIR} in
	    *-mesh_0.37M*)
		decomposeParDictArray=(
mpi_00032-method_scotch
mpi_00064-method_scotch
mpi_00128-method_scotch
mpi_00256-method_scotch
		)
		;;
	    *-mesh_3M*)
		decomposeParDictArray=(
mpi_00032-method_scotch
mpi_00064-method_scotch
mpi_00128-method_scotch
mpi_00256-method_scotch
		)
		;;
	    *-mesh_24M*)
		decomposeParDictArray=(
mpi_00032-method_scotch
mpi_00064-method_scotch
mpi_00128-method_scotch
mpi_00256-method_scotch
		)
		;;
	esac
	solveBatchArray=(
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI5_1_3_258-LTU
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI5_1_3_258-TU
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2017_3_196-LTU
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2017_3_196-TU
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-LTU
	    n32-2.3.0-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-TU
	    n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI1_10_7-
	    n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI1_10_7-b
	    n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI1_10_7-br
	    n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI1_10_7-s
	    n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1-
	    n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1-b
	    n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1-br
	    n32-2.3.0-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1-s
	)
   ;;
    n36)
	case ${WORKDIR} in
	    *-mesh_0.37M*)
		decomposeParDictArray=(
mpi_00036-method_scotch
mpi_00072-method_scotch
mpi_00144-method_scotch
mpi_00288-method_scotch
	)
		;;
	    *-mesh_3M*)
		decomposeParDictArray=(
mpi_00036-method_scotch
mpi_00072-method_scotch
mpi_00144-method_scotch
mpi_00288-method_scotch
	)
		;;
	    *-mesh_24M*)
		decomposeParDictArray=(
mpi_00036-method_scotch
mpi_00072-method_scotch
mpi_00144-method_scotch
mpi_00288-method_scotch
		)
		;;
	esac
	solveBatchArray=(
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2017_3_196-6DFLTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2017_3_196-6DFTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2017_3_196-6FLTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2017_3_196-6FTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-6DFLTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-6DTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-6FLTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-6FTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_3_222-6DF
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_3_222-6DFL
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_3_222-6DFLTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_3_222-6DFTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_3_222-6F
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_3_222-6FL
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_3_222-6FLTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_3_222-6FTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI5_1_3_258-6DFLTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI5_1_3_258-6DFTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI5_1_3_258-6FLTU
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI5_1_3_258-6FTU
	    n36-v1806-system-Gcc4_8_5-Opt-SYSTEMOPENMPI1_10_7-DMR
	    n36-v1806-system-Gcc4_8_5-Opt-SYSTEMOPENMPI1_10_7-DMRb
	    n36-v1806-system-Gcc4_8_5-Opt-SYSTEMOPENMPI1_10_7-DMRbr
	    n36-v1806-system-Gcc4_8_5-Opt-SYSTEMOPENMPI1_10_7-DMRs
	    n36-v1806-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1-DMR
	    n36-v1806-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1-DMRb
	    n36-v1806-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1-DMRbr
	    n36-v1806-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1-DMRs
	    n36-v1806-system-Icc2016_4_258-Opt-INTELMPI5_1_3_258-6FTU
	    n36-v1806-system-Icc2017_4_196-Opt-INTELMPI2017_3_196-6FTU
	    n36-v1806-system-Icc2018_1_163-Opt-INTELMPI2018_1_163-6FTU
	    n36-v1806-system-Icc2018_3_222-Opt-INTELMPI2018_3_222-6F
)
   ;;
esac

fvSolutionArray=(
PCG-DIC
)

BATCH_PRE=0
BATCH_DECOMPOSEPAR=0
BATCH_SOLVE=1

MAX_NUMBER_OF_QUEUE=128

NumberOfBatchQueue()
{
    pjstat | grep "^[0-9]" | wc -l
}

BatchSubmit()
{
    local BATCHFILE=$1
    local MPI=$2

    echo "BATCHFILE: $BATCHFILE" 
    echo "MPI: $MPI" 

    if [ "$BATCHFILE" = "pre.sh" -o "$BATCHFILE" = "decomposePar.sh" ];then
        local NPROCS_PER_NODE=1
        local NODE=1
    else
        local NPROCS_PER_NODE=`echo $BATCHFILE | cut  -d "-"  -f 1 | tr -d 'n'`
        local NODE=`echo "($MPI + $NPROCS_PER_NODE - 1)/ $NPROCS_PER_NODE" | bc`
    fi

    echo "NPROCS_PER_NODE: $NPROCS_PER_NODE" 
    echo "NODE: $NODE" 

    if [ "$BATCHFILE" != "pre.sh" -a "$BATCHFILE" != "decomposePar.sh" ];then
	local MPI_TMP=`echo "$NODE * $NPROCS_PER_NODE" | bc`
	echo "NODE * NPROCS_PER_NODE: $MPI_TMP"
        if [ $MPI -ne $MPI_TMP ];then
	    echo "Number of MPI is mismatch. Skip running."
  	    return 0
	fi  
    fi

    if [ $NODE -eq 1 ]
    then
      rscgrp=ito-ss-dbg
    elif [ $NODE -le 4 ]
    then
      rscgrp=ito-s-dbg
    elif [ $NODE -le 16 ]
    then
      rscgrp=ito-m-dbg
    elif [ $NODE -le 64 ]
    then
      rscgrp=ito-l
    elif [ $NODE -le 128 ]
    then
      rscgrp=ito-xl
    else
      rscgrp=ito-xxl
    fi

    command="pjsub -j -S -L rscgrp=$rscgrp,rscunit=ito-a,vnode=${NODE},vnode-core=36,elapse=${WALLTIME} --mpi proc=${MPI} ${BATCHFILE}"
    echo $command
    $command
}
