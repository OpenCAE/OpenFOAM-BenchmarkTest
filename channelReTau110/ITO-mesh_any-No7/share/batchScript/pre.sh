#!/bin/bash

# OpenFOAM-v1706, ThirdParty Gcc, openmpi-1.10.4
. /home/app/a/OpenFOAM/OpenFOAM-v1706/etc/bashrc

# Application name
application=pre.sh

# Log file
log=log.${application}.${PJM_JOBID}
batchFileDone=${application}.done

env > $log 2>&1
blockMesh >> $log 2>&1
grep "^End" $log >& /dev/null && touch $batchFileDone

#------------------------------------------------------------------------------
