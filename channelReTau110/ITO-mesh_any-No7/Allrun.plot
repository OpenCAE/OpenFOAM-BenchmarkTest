#!/bin/sh

maxNumberOfSampling=5

figs=""
for batch in \
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI5_1_3_258-6 \
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2017_3_196-6 \
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_1_163-6 \
	    n36-v1806-system-Gcc4_8_5-Opt-INTELMPI2018_3_222-6 \
	    n36-v1806-system-Gcc4_8_5-Opt-SYSTEMOPENMPI1_10_7-DMR \
	    n36-v1806-system-Gcc4_8_5-Opt-SYSTEMOPENMPI3_1_1-DMR \
    	    n36-v1806-system-Icc \

do
    echo $batch
    tempfile=$(mktemp)
    head -n 1 all.csv > $tempfile
    grep $batch all.csv >> $tempfile
    batchFileName=$(cut -d ',' -f 4 $tempfile | sort | uniq | grep $batch)
    fig=`python ../bin/plot.py $tempfile \
        -L 1 \
	-m $maxNumberOfSampling \
	--sph \
	--ExecutionTimePerStep ExecutionTimePerStep \
	--tickFontSize 8 \
	-b $batchFileName;\
        python ../bin/plot.py $tempfile \
        -L 1 \
	-m $maxNumberOfSampling \
	--pe -y \
	--ExecutionTimePerStep ExecutionTimePerStep \
	--tickFontSize 8 \
	-b $batchFileName`
    figs="$figs $fig"
done

base=`basename ${PWD}`
CONVERT=$(which convert 2> /dev/null)
if [ "x$CONVERT" != "x" ]
then
    $CONVERT -density 300 ${figs} ${base}.pdf
    rm -f ${figs}
else
    echo "convert not installed" >&2
fi
