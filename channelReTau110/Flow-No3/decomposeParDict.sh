#!/bin/bash
#
MAX_NUMBER_OF_QUEUE=100
# 最適化スクリプトで生成されたパラメータ定義ファイルを評価
source $0.param
# ppn(process per node)とノード数から，領域分割数を算出
numberOfSubdomains=`expr $decomposeParDict_ppn \* $decomposeParDict_node`
# 元のケースディレクトリから必要なディレクトリをリンク
if [ ! -d include ]
then
    mkdir include
    (cd include
	ln -s  ../../include/* ./
    )
fi
for file in 0 constant system
do
    [ -L $file ] || ln -s ../$file .
done
if [ $numberOfSubdomains -eq 1 ]
then
    JOBID=00000
    echo "End" > log.decomposeParDict.sh.$JOBID
    echo $JOBID
    exit 0
fi
# decomposeParDictを生成
rm -f include/decomposeParDict
cat > include/decomposeParDict <<EOF
numberOfSubdomains $numberOfSubdomains;
method $decomposeParDict_method;
preservePatches (sides_half0 sides_half1 inout_half0 inout_half1);
multiLevelCoeffs
{
  method  scotch;
  domains ($decomposeParDict_ppn $decomposeParDict_node);
}
EOF
cat > decomposePar.sh <<EOF
#!/bin/sh
#PJM -L rscunit=cl
#PJM -L rscgrp=cl-share
#PJM -L elapse=4:00:00
#PJM --mpi proc=1
#PJM -S
#PJM -j
module purge
module load gcc/4.8.5
module unload openmpi
module load impi/2019.7.217
module load openfoam/v2006
source \${WM_PROJECT_DIR}/etc/bashrc
decomposePar >& log.$0.\$PJM_JOBID
EOF
echo "" > include/OptimisationSwitches
while :
do
    nq=`pjstat | grep "^[0-9]" | wc -l`
    [ "$nq" -lt "$MAX_NUMBER_OF_QUEUE" ] &&  break
    sleep 5
done
JOBID=$(ssh flow-cloud.cc.nagoya-u.ac.jp "cd $PWD;pjsub -z jid decomposePar.sh")
echo $JOBID
exit 0
