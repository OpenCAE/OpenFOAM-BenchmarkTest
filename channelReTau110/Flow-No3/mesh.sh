#!/bin/bash
#
MAX_NUMBER_OF_QUEUE=100
# 最適化スクリプトで生成されたパラメータ定義ファイルを評価
source $0.param
# 元のケースディレクトリから必要なディレクトリをリンク
for dir in constant include
do
    if [ ! -d $dir ]
    then
	mkdir $dir
	(cd $dir
	    ln -s  ../../../$dir/* ./
	)
    fi
done
rm -f constant/polyMesh
for file in 0 system
do
    [ -L $file ] || ln -s ../../$file .
done
# Delta time and end time
case $mx in
    120)
	deltaT=0.004
	endTime=0.204
	;;
    240)
	deltaT=0.002
	endTime=0.102
	;;
    480)
	deltaT=0.001
	endTime=0.009
	;;
esac
# controlDictを生成
rm -f include/controlDict
cat > include/controlDict <<EOF
application pimpleFoam;
startFrom startTime;
startTime 0;
stopAt endTime;
writeControl runTime;
deltaT $deltaT;
endTime $endTime;
writeInterval 1;
purgeWrite 0;
writeFormat ascii;
writePrecision 16;
writeCompression off;
timeFormat general;
timePrecision 6;
runTimeModifiable false;
EOF
# blockMeshDictを生成
rm -f include/blockMeshDict
cat > include/blockMeshDict <<EOF
convertToMeters 1;
vertices
(
  (0 0 0 )
  (15.70796326794896619220 0 0 )
  (15.70796326794896619220 2 0 )
  (0 2 0 )
  (0 0 6.28318530717958647688)
  (15.70796326794896619220 0 6.28318530717958647688)
  (15.70796326794896619220 2 6.28318530717958647688)
  (0 2 6.28318530717958647688)
);
blocks
(
  hex (0 1 2 3 4 5 6 7)
  ($mx $my $mz)
  simpleGrading (1 1 1)
);
edges
(
);
boundary
(
  bottomWall
  {
    type wall;
     faces ((1 5 4 0));
  }
  topWall
  {
    type wall;
    faces ((2 6 7 3));
  }
  sides_half0
  {
    type cyclic;
    neighbourPatch sides_half1;
    faces ((0 3 2 1));
  }
  sides_half1
  {
    type cyclic;
    neighbourPatch sides_half0;
    faces ((4 7 6 5));
  }
  inout_half0
  {
    type cyclic;
    neighbourPatch inout_half1;
     faces ((0 4 7 3));
  }
  inout_half1
  {
    type cyclic;
    neighbourPatch inout_half0;
    faces ((1 5 6 2));
  }
);
mergePatchPairs
(
);
EOF
cat > blockMesh.sh <<EOF
#!/bin/sh
#PJM -L rscunit=cl
#PJM -L rscgrp=cl-share
#PJM -L elapse=0:10:00
#PJM --mpi proc=1
#PJM -S
#PJM -j
module purge
module load gcc/4.8.5
module unload openmpi
module load impi/2019.7.217
module load openfoam/v2006
source \${WM_PROJECT_DIR}/etc/bashrc
blockMesh >& log.$0.\$PJM_JOBID
EOF
echo "" > include/OptimisationSwitches
while :
do
    nq=`pjstat | grep "^[0-9]" | wc -l`
    [ "$nq" -lt "$MAX_NUMBER_OF_QUEUE" ] &&  break
    sleep 5
done
JOBID=$(ssh flow-cloud.cc.nagoya-u.ac.jp "cd $PWD;pjsub -z jid blockMesh.sh")
echo $JOBID
exit 0
