# Flow-No3

## General information

* Measurer: IMANO Masashi
* Date: Apr 2021

## Hardware

* Name: Flow Type II (http://www.icts.nagoya-u.ac.jp/en/sc/)

## Compiler
* CUDA 11.2.1 (cuda/11.2.1 module)

## MPI
* OpenMPI 4.0.5 (openmpi_cuda/4.0.5 module)

## WM_PRECISION_OPTION
* DP
* SP
