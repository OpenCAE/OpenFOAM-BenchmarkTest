# Oracle_Cloud_BM.HPC2.36-mesh_3M-No3

## General information

* Measurer:  Masashi Imano
* Date: Nov 2018

## Benchmark condition

* Number of mesh：2995200(3M)
* Number of nodes: 1-26
* Number of processors per node: 32, 36
* Number of trials: 5

## Other information

See https://gitlab.com/OpenCAE/OpenFOAM-BenchmarkTest/blob/master/channelReTau110/Oracle_Cloud_BM.HPC2.36-mesh_any-No3/README.md
