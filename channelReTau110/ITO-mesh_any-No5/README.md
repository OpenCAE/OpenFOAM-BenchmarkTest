# ITO-mesh_any-No5

## Hardware

* Name: ITO Subsystem A (https://www.cc.kyushu-u.ac.jp/scp/system/ITO/)

## Solve batch script 

Solve batch script name(legend of plot):

nPPN-FOAM_VERSION-COMPILER_TYPE-COMPILER-COMPILE_OPTION-MPLIB-OPTIONS

### PPN

Number of processes per node.

* 36

### FOAM_VERSION

OpenFOAM version.

* 2.3.0: OpenFOAM-2.3.0

### COMPILER_TYPE

* system: System compiler

### COMPILER

* Gcc4_8_5: Gcc 4.8.5

### COMPILE_OPTION

* Opt: -O3 (default)

### MPLIB

* INTELMPI2018_1_163: Intel MPI 2018.1.163

### OPTIONS

#### mpirun options for Intel MPI

* Default settings:

```
unset I_MPI_PIN_DOMAIN
export I_MPI_FALLBACK=0
export I_MPI_DEBUG=6
export I_MPI_PERHOST=${PPN}
export I_MPI_HYDRA_BOOTSTRAP=rsh
export I_MPI_HYDRA_BOOTSTRAP_EXEC=/bin/pjrsh
export I_MPI_HYDRA_HOST_FILE="${PJM_O_NODEINF}"
```

* Additional settings: 
  * L:
    * PPN=36: -genv I_MPI_PIN_PROCESSOR_LIST=18-35,0-17
    * PPN=32: -genv I_MPI_PIN_PROCESSOR_LIST=18-31,0-15
  * T: export I_MPI_DAPL_TRANSLATION_CACHE=0
  * U: export I_MPI_DAPL_UD_TRANSLATION_CACHE=0

#### mpirun options for OpenMPI

* Default options:
```
--report-bindings --display-map -display-devel-map --mca plm_rsh_agent /bin/pjrsh -machinefile $PJM_O_NODEINF 
```

* Additional options: 
  * b:
    * --bind-to none --npernode $PPN 
    * Set BIND_MODE=1 and use process binding shell script
  * r: Set REVERSE_SOCKETID=1 in the process binding shell script
  * s:
    * PPN=36: --map-by ppr:18:socket 
    * PPN=32: --map-by ppr:16:socket 

* Process binding shell script
```
#!/bin/bash

# process binding
BIND_MODE=$BIND_MODE
REVERSE_SOCKETID=$REVERSE_SOCKETID
MPI_PERHOST=$PPN

# number of cores per CPU and NODE
CORES1=18
CORES2=36

if [ -n "$MV2_COMM_WORLD_LOCAL_RANK" ]
then
    RANK=${MV2_COMM_WORLD_LOCAL_RANK}
elif [ -n "$OMPI_COMM_WORLD_RANK" ]
then
    RANK=${OMPI_COMM_WORLD_RANK}
elif [ -n "$PMI_RANK" ]
then
    RANK=${PMI_RANK}
fi

if [ $MPI_PERHOST -ge 2 ]
then
    HALFCORE=$(( ${MPI_PERHOST} / 2 ))
else
    HALFCORE=1
fi

# localrank = rank in each node : 0 ~ i_mpi_perhost-1
# localrank2 = rank in each socket : 0 ~ (i_mpi_perhost/2)-1
LOCALRANK=$(( ${RANK} % ${MPI_PERHOST} ))
LOCALRANK2=$(( ${RANK} % ${HALFCORE} ))

# socketID = localrank / 2 : 0 or 1
SOCKETID=$(( ${LOCALRANK} / ${HALFCORE} ))

# offset = reverse-sockid * cores per cpu : 0 or 18
if [ $REVERSE_SOCKETID -eq 1 ]
then
  OFFSET=$(( ( 1 - ${SOCKETID} ) * ${CORES1} ))
else
  OFFSET=$(( ${SOCKETID} * ${CORES1} ))
fi

# phycpunode = offset + localrank2*localsize
P=$(( ${OFFSET} + ${LOCALRANK2} ))

run_command="numactl --physcpubind=${P} --localalloc pimpleFoam -parallel"
$run_command
```

## How to plot results

Link settings and shell scripts if Allrun.plot does not exists.

```bash
../bin/linkSettings.sh ITO-mesh_any-No5
```

Execute Allrun.plot

```bash
./Allrun.plot
```

## Note
