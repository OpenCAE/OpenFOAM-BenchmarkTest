#!/bin/bash

function generate_fvSolution () {
    if [  "$fvSolution_solvers_p_solver" = "PCG" -o "$fvSolution_solvers_p_solver" = "PPCG" -o "$fvSolution_solvers_p_solver" = "PPCR" ]
    then
	if [ "$fvSolution_solvers_p_preconditioner" = "GAMG" ]
	then
	    fvSolution_solvers_p_preconditioner_smoother=`cat << EOM
        preconditioner {
            preconditioner  $fvSolution_solvers_p_preconditioner;
            smoother $fvSolution_solvers_p_smoother;
            agglomerator $fvSolution_solvers_p_agglomerator;
            cacheAgglomeration $fvSolution_solvers_p_cacheAgglomeration;
            interpolateCorrection $fvSolution_solvers_p_interpolateCorrection;
            maxPostSweeps $fvSolution_solvers_p_maxPostSweeps;
            maxPreSweeps $fvSolution_solvers_p_maxPreSweeps;
            mergeLevels $fvSolution_solvers_p_mergeLevels;
            nCellsInCoarsestLevel $fvSolution_solvers_p_nCellsInCoarsestLevel;
            nFinestSweeps $fvSolution_solvers_p_nFinestSweeps;
            nPostSweeps $fvSolution_solvers_p_nPostSweeps;
            nPreSweeps $fvSolution_solvers_p_nPreSweeps;
            postSweepsLevelMultiplier $fvSolution_solvers_p_postSweepsLevelMultiplier;
            preSweepsLevelMultiplier $fvSolution_solvers_p_preSweepsLevelMultiplier;
            directSolveCoarsest $fvSolution_solvers_p_directSolveCoarsest;
            nVcycles $fvSolution_solvers_p_nVcycles;
        }
EOM
`
	else
	    fvSolution_solvers_p_preconditioner_smoother="preconditioner $fvSolution_solvers_p_preconditioner;"
	fi
    elif [ "$fvSolution_solvers_p_solver" = "GAMG" ]
    then
	fvSolution_solvers_p_preconditioner_smoother=`cat << EOM
        smoother $fvSolution_solvers_p_smoother;
        agglomerator $fvSolution_solvers_p_agglomerator;
        cacheAgglomeration $fvSolution_solvers_p_cacheAgglomeration;
        interpolateCorrection $fvSolution_solvers_p_interpolateCorrection;
        maxPostSweeps $fvSolution_solvers_p_maxPostSweeps;
        maxPreSweeps $fvSolution_solvers_p_maxPreSweeps;
        mergeLevels $fvSolution_solvers_p_mergeLevels;
        nCellsInCoarsestLevel $fvSolution_solvers_p_nCellsInCoarsestLevel;
        nFinestSweeps $fvSolution_solvers_p_nFinestSweeps;
        nPostSweeps $fvSolution_solvers_p_nPostSweeps;
        nPreSweeps $fvSolution_solvers_p_nPreSweeps;
        postSweepsLevelMultiplier $fvSolution_solvers_p_postSweepsLevelMultiplier;
        preSweepsLevelMultiplier $fvSolution_solvers_p_preSweepsLevelMultiplier;
        directSolveCoarsest $fvSolution_solvers_p_directSolveCoarsest;
        coarsestLevelCorr
        {
            preconditioner DIC;
            solver $fvSolution_solvers_p_coarsestLevelCorr_solver;
            relTol $fvSolution_solvers_p_coarsestLevelCorr_relTol;
        }
EOM
`
    fi
    if [ "$fvSolution_solvers_pFinal_solver" = "PCG" -o "$fvSolution_solvers_pFinal_solver" = "PPCG" -o "$fvSolution_solvers_pFinal_solver" = "PPCR" ]
    then
	if [ "$fvSolution_solvers_pFinal_preconditioner" = "GAMG" ]
	then
	    fvSolution_solvers_pFinal_preconditioner_smoother=`cat << EOM
        preconditioner {
            preconditioner  $fvSolution_solvers_pFinal_preconditioner;
            smoother $fvSolution_solvers_pFinal_smoother;
            agglomerator $fvSolution_solvers_pFinal_agglomerator;
            cacheAgglomeration $fvSolution_solvers_pFinal_cacheAgglomeration;
            interpolateCorrection $fvSolution_solvers_pFinal_interpolateCorrection;
            maxPostSweeps $fvSolution_solvers_pFinal_maxPostSweeps;
            maxPreSweeps $fvSolution_solvers_pFinal_maxPreSweeps;
            mergeLevels $fvSolution_solvers_pFinal_mergeLevels;
            nCellsInCoarsestLevel $fvSolution_solvers_pFinal_nCellsInCoarsestLevel;
            nFinestSweeps $fvSolution_solvers_pFinal_nFinestSweeps;
            nPostSweeps $fvSolution_solvers_pFinal_nPostSweeps;
            nPreSweeps $fvSolution_solvers_pFinal_nPreSweeps;
            postSweepsLevelMultiplier $fvSolution_solvers_pFinal_postSweepsLevelMultiplier;
            preSweepsLevelMultiplier $fvSolution_solvers_pFinal_preSweepsLevelMultiplier;
            directSolveCoarsest $fvSolution_solvers_pFinal_directSolveCoarsest;
            nVcycles $fvSolution_solvers_pFinal_nVcycles;
        }
EOM
`
	else
	    fvSolution_solvers_pFinal_preconditioner_smoother="preconditioner $fvSolution_solvers_pFinal_preconditioner;"
	fi
    elif [ "$fvSolution_solvers_pFinal_solver" = "GAMG" ]
    then
	fvSolution_solvers_pFinal_preconditioner_smoother=`cat << EOM
        smoother $fvSolution_solvers_pFinal_smoother;
        agglomerator $fvSolution_solvers_pFinal_agglomerator;
        cacheAgglomeration $fvSolution_solvers_pFinal_cacheAgglomeration;
        interpolateCorrection $fvSolution_solvers_pFinal_interpolateCorrection;
        maxPostSweeps $fvSolution_solvers_pFinal_maxPostSweeps;
        maxPreSweeps $fvSolution_solvers_pFinal_maxPreSweeps;
        mergeLevels $fvSolution_solvers_pFinal_mergeLevels;
        nCellsInCoarsestLevel $fvSolution_solvers_pFinal_nCellsInCoarsestLevel;
        nFinestSweeps $fvSolution_solvers_pFinal_nFinestSweeps;
        nPostSweeps $fvSolution_solvers_pFinal_nPostSweeps;
        nPreSweeps $fvSolution_solvers_pFinal_nPreSweeps;
        postSweepsLevelMultiplier $fvSolution_solvers_pFinal_postSweepsLevelMultiplier;
        preSweepsLevelMultiplier $fvSolution_solvers_pFinal_preSweepsLevelMultiplier;
        directSolveCoarsest $fvSolution_solvers_pFinal_directSolveCoarsest;
        coarsestLevelCorr
        {
            preconditioner DIC;
            solver $fvSolution_solvers_pFinal_coarsestLevelCorr_solver;
            relTol $fvSolution_solvers_pFinal_coarsestLevelCorr_relTol;
        }
EOM
`
    fi
    if [ "$fvSolution_solvers_UFinal_solver" = "smoothSolver" ]
    then
	fvSolution_solvers_UFinal_preconditioner_smoother="smoother $fvSolution_solvers_UFinal_smoother;"
    elif [ "$fvSolution_solvers_UFinal_solver" = "PBiCG" -o "$fvSolution_solvers_UFinal_solver" = "PBiCGStab" ]
    then
	fvSolution_solvers_UFinal_preconditioner_smoother="preconditioner $fvSolution_solvers_UFinal_preconditioner;"
    fi
    rm -f include/fvSolution
    cat > include/fvSolution <<EOF
solvers
{
  p
  {
    maxIter 5000;
    tolerance 1e-6;
    relTol $fvSolution_solvers_p_relTol;
    solver $fvSolution_solvers_p_solver;
$fvSolution_solvers_p_preconditioner_smoother
  }
  pFinal
  {
    maxIter 5000;
    tolerance 1e-6;
    relTol 0;
    solver $fvSolution_solvers_pFinal_solver;
$fvSolution_solvers_pFinal_preconditioner_smoother
  }
  UFinal
  {
    maxIter 5000;
    tolerance 1e-05;
    relTol 0;
    solver $fvSolution_solvers_UFinal_solver;
$fvSolution_solvers_UFinal_preconditioner_smoother
  }
}
PIMPLE
{
  nOuterCorrectors 1;
  nCorrectors 2;
  nNonOrthogonalCorrectors 0;
  pRefCell 0;
  pRefValue 0;
}
EOF
}

function generate_batch_and_include_files_flow_fx () {
    if [ $decomposeParDict_node -le 12 ]
    then
	rscgrp=fx-middle2
    elif [ $decomposeParDict_node -le 96 ]
    then
	rscgrp=fx-middle
    elif [ $decomposeParDict_node -le 192 ]
    then
	rscgrp=fx-large
    elif [ $decomposeParDict_node -le 768 ]
    then
	rscgrp=fx-xlarge
    else
	rscgrp=fx-special
    fi
    (
	cat <<EOF
#!/bin/sh
#PJM -L rscunit=fx
#PJM -L rscgrp=$rscgrp
#PJM -L node=$decomposeParDict_node
#PJM -L elapse=0:10:00
#PJM --mpi proc=$numberOfSubdomains
#PJM -S
#PJM -j
module purge
module load tcs/$tcs_version
EOF
	case $OpenFOAM_VERSION in
	    openfoam/*)
		cat <<EOF
module load $OpenFOAM_VERSION
source \$WM_PROJECT_DIR/etc/bashrc \\
EOF
		;;
	    OpenFOAM-*)
		cat <<EOF
source \$HOME/OpenFOAM/$OpenFOAM_VERSION/etc/bashrc \\
EOF
		;;
	esac
	cat <<EOF
WM_COMPILER_TYPE=$WM_COMPILER_TYPE \
WM_COMPILER=$WM_COMPILER \
WM_PRECISION_OPTION=$WM_PRECISION_OPTION \
WM_LABEL_SIZE=$WM_LABEL_SIZE \
WM_COMPILE_OPTION=$WM_COMPILE_OPTION \
WM_MPLIB=$WM_MPLIB
EOF
	if [ "$profiling" = "fipp" ]
	then
	    cat <<EOF
fipp -C -d./fipp.\$PJM_JOBID \\
EOF
	elif [ "$profiling" = "vtune" ]
	then
	    cat <<EOF
module load vtune/2020.1.0.607630
amplxe-cl -collect hotspots -knob sampling-mode=sw -r vtune.\$PJM_JOBID -- \\
EOF
	fi
	cat <<EOF
mpiexec -of log.$0.\$PJM_JOBID -n \$PJM_MPI_PROC pimpleFoam -parallel
EOF
    ) > run.sh
}


function generate_batch_and_include_files_flow_cx () {
    case "$WM_COMPILER" in
	Nvcc10_1)
	    WM_COMPILER_COMMAND="module load cuda/10.1.243"
	    ;;
	Nvcc10_2)
	    WM_COMPILER_COMMAND="module load cuda/10.2.89_440.33.01"
	    ;;
	Nvcc11_2_1Gcc4_8_5)
	    WM_COMPILER_COMMAND="module purge;module load cuda/11.2.1"
	    ;;
	Nvcc11_2_1Gcc8_4_0)
	    WM_COMPILER_COMMAND="module purge;module load gcc/8.4.0 cuda/11.2.1"
	    ;;
    esac
    case "$WM_MPLIB" in
	SYSTEMOPENMPI4_0_3)
	    WM_MPLIB_COMMAND="module load gcc/4.8.5;module load openmpi/4.0.3"
	    ;;
	SYSTEMOPENMPI4_0_5_cuda)
	    WM_MPLIB_COMMAND="module load openmpi_cuda/4.0.5;module load ucx_cuda/1.10.0"
	    ;;
    esac
    if [ $numberOfSubdomains -eq 1 ]
    then
	rscgrp=cx-share
    else
	rscgrp=cx-small
    fi
    case $policy in
	compact)
	    deviceArray=("0" "1" "2" "3")
	    ;;
	scatter)
	    deviceArray=("0" "2" "1" "3")
	    ;;
    esac
    devices="'("
    i=0
    startdevice=0
    deviceI=$startdevice
    while [ "$i" -lt "$numberOfSubdomains" ]
    do
	devices="$devices ${deviceArray[$deviceI]}"
	i=$(expr $i + 1)
	deviceI=$(expr $deviceI + 1)
	if [ "$deviceI" -eq $decomposeParDict_ppn ];then
	    deviceI=$startdevice
	fi
    done
    devices="$devices )'"
    (
	cat <<EOF
#!/bin/bash
module purge
$WM_COMPILER_COMMAND
$WM_MPLIB_COMMAND
source $HOME/RapidCFD/$OpenFOAM_VERSION/etc/bashrc \
WM_COMPILER_TYPE=$WM_COMPILER_TYPE \
WM_COMPILER=$WM_COMPILER \
WM_PRECISION_OPTION=$WM_PRECISION_OPTION \
WM_LABEL_SIZE=$WM_LABEL_SIZE \
WM_COMPILE_OPTION=$WM_COMPILE_OPTION \
WM_MPLIB=$WM_MPLIB
pimpleFoam -parallel -devices $devices
EOF
    ) > application.sh
    chmod +x application.sh
    (
	cat <<EOF
#!/bin/sh
#PJM -L rscunit=cx
#PJM -L rscgrp=$rscgrp
EOF
	if [ $numberOfSubdomains -eq 1 ]
	then
	    cat <<EOF
#PJM -L gpu=1
EOF
	else
	    cat <<EOF
#PJM -L node=$decomposeParDict_node
EOF
	fi
	cat <<EOF
#PJM -L elapse=1:00:00
#PJM --mpi proc=$numberOfSubdomains
#PJM -S
#PJM -j
module purge
$WM_COMPILER_COMMAND
$WM_MPLIB_COMMAND
source $HOME/RapidCFD/$OpenFOAM_VERSION/etc/bashrc \
WM_COMPILER_TYPE=$WM_COMPILER_TYPE \
WM_COMPILER=$WM_COMPILER \
WM_PRECISION_OPTION=$WM_PRECISION_OPTION \
WM_LABEL_SIZE=$WM_LABEL_SIZE \
WM_COMPILE_OPTION=$WM_COMPILE_OPTION \
WM_MPLIB=$WM_MPLIB
EOF
	if [ "$profiling" = "vtune" ]
	then
	    cat <<EOF
module load vtune/2020.1.0.607630
vtune -collect hotspots -knob sampling-mode=sw -r vtune.\$PJM_JOBID -- \\
EOF
	fi
	if [ $numberOfSubdomains -eq 1 ]
	then
	    cat <<EOF
pimpleFoam >& log.$0.\$PJM_JOBID 
EOF
	else
	    cat <<EOF
mpiexec -display-map -display-devel-map -n \$PJM_MPI_PROC -machinefile \$PJM_O_NODEINF -npernode $decomposeParDict_ppn ./application.sh >& log.$0.\$PJM_JOBID 
EOF
	fi
    ) > run.sh
    rm -f include/OptimisationSwitches
    cat > include/OptimisationSwitches <<EOF
OptimisationSwitches
{
  gpuDirectTransfer $gpuDirectTransfer;
  favourSpeedOverMemory $favourSpeedOverMemory;
}
EOF
}


function generate_batch_and_include_files_flow_cl () {
    if [ $decomposeParDict_ppn -le 20 ]
    then
	rscgrp=cl-share
    else
	rscgrp=cl-extra
    fi
    (
	cat <<EOF
#!/bin/sh
#PJM -L rscunit=cl
#PJM -L rscgrp=$rscgrp
EOF
	if [ "$rscgrp" !=  "cl-share" ]
	then
	    cat <<EOF
#PJM -L node=$decomposeParDict_node
EOF
fi
	cat <<EOF
#PJM -L elapse=0:05:00
#PJM --mpi proc=$numberOfSubdomains
#PJM -S
#PJM -j
module purge
module purge;module load gcc/4.8.5;module unload openmpi;module load impi/2019.7.217
source $HOME/OpenFOAM/$OpenFOAM_VERSION/etc/bashrc \
WM_COMPILER_TYPE=$WM_COMPILER_TYPE \
WM_COMPILER=$WM_COMPILER \
WM_PRECISION_OPTION=$WM_PRECISION_OPTION \
WM_LABEL_SIZE=$WM_LABEL_SIZE \
WM_COMPILE_OPTION=$WM_COMPILE_OPTION \
WM_MPLIB=$WM_MPLIB
EOF
	if [ "$profiling" = "vtune" ]
	then
	    cat <<EOF
module load vtune/2020.1.0.607630
export I_MPI_GTOOL="vtune -collect hotspots -knob sampling-mode=sw -r vtune.\$PJM_JOBID:all=node-wide"
EOF
	fi
	cat <<EOF
mpiexec -n \$PJM_MPI_PROC pimpleFoam -parallel >& log.$0.\$PJM_JOBID 
EOF
    ) > run.sh
}

#
# Main
#
MAX_NUMBER_OF_QUEUE=100
# 最適化スクリプトで生成されたdecomposeParDict.sh向けのパラメータ定義ファイルを評価
source ../decomposeParDict.sh.param
# 最適化スクリプトで生成されたパラメータ定義ファイルを評価
source $0.param
# ppn(process per node)とノード数から，領域分割数を算出
numberOfSubdomains=`expr $decomposeParDict_ppn \* $decomposeParDict_node`
# 元のケースディレクトリから必要なディレクトリをリンク
if [ ! -d include ]
then
    mkdir include
    (cd include
	ln -s  ../../include/* ./
    )
fi
for file in 0 constant system
do
    [ -L $file ] || ln -s ../$file .
done
# 領域分割したケースディレクトリからプロセッサディレクトリをリンク
if [ $numberOfSubdomains -gt 1 ]
then 
    i=0
    while [ $i -lt $numberOfSubdomains ] 
    do
	file=processor$i
	[ -L $file ] || ln -s ../$file .
	i=`expr $i + 1` 
    done
fi
# Generate fvSolution
generate_fvSolution
# Generage batch and include files
hostname=$(hostname)
case $hostname in 
    flow-fx*)
	generate_batch_and_include_files_flow_fx
	;;
    flow-cx*)
	generate_batch_and_include_files_flow_cx
	;;
    flow-cl*)
	generate_batch_and_include_files_flow_cl
	;;
esac
# Submit job
while :
do
    nq=`pjstat | grep "^[0-9]" | wc -l`
    [ "$nq" -lt "$MAX_NUMBER_OF_QUEUE" ] &&  break
    sleep 5
done
JOBID=$(pjsub -z jid run.sh)
echo $JOBID
# Waiting for job
log=log.$0.$JOBID
state=run.sh.$JOBID.stats
while :
do
    if [ -f $log ]
    then
	if grep 'No Iterations 500[01]' $log >& /dev/null -o
	    grep 'ERROR' $log >& /dev/null
	then
	    pjdel $JOBID >& /dev/null
	    echo "Killed" >> $log
	    exit 1
	elif grep '^End' $log >& /dev/null
	then
	    exit 0
	fi
	sleep 3
	if [ -f $state ]
	then
	    echo "Exit" >> $log
	    exit 1
	fi
    else
	sleep 3
    fi
done
exit 0
