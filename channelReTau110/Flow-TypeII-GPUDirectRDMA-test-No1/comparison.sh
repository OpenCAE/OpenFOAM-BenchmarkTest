#!/bin/bash
source tuneOpenFOAMGridsearch.param
awk -v steps=$steps -F ',' 'BEGIN {print "#PPN,ExecutionTimePerStep,nNodes,solveBatch,fvSolution,Steps,ExecutionTimeFirstStep"} {if ($3==4) {printf "%d,%g,%d,%s-%s,%s-%s-%s-%s-%s,%d,0\n",$3,$1,$2,$7,$10,$14,$15,$16,$17,$21,steps}}' tuneOpenFOAM.json.csv > comparison.csv
