# Azure_H16r-mesh_3M-No1

## 測定者情報

* 測定者: 五十木 秀一 (Shuichi Gojuki: shugo@microsoft.com)
* 測定日: 2017年6月6日

## ベンチマーク情報

* 格子数(格子数倍率)：2995200(3M)
* MPI数(ノード数)：16(1), 32(2), 64(4), 128(8), 192(12), 256(16)

## ハードウェア情報

* ハードウェア名称: [Azure H16r インスタンス](https://docs.microsoft.com/ja-jp/azure/cloud-services/cloud-services-sizes-specs#h-series)
* CPUの種別: Intel Xeon E5-2667v3
* CPUの周波数: 3.20GHz
* コア数/CPU: 8
* CPU数/ノード: 2
* メモリ量/ノード: 112GB
* インターフェース種別: Infiniband FDR
* インターフェース数/ノード: 1基
* インターフェース・スループット/基: 56Gbps

## ソフトウェア情報

* OS: SUSE Linux Enterprise Server 12 SP1 (x86_64)
* OpenFOAMのバージョン: 2.3.0
* ビルドに使用したコンパイラ: gcc version 4.8.5 (SUSE Linux)
* コンパイラの最適化オプション: -O3
* 使用したMPIライブラリ: Intel MPI 5.0.3.048
