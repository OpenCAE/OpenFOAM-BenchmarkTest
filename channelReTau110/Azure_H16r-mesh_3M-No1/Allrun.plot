#!/bin/sh

figs=`python ../bin/plot.py --sph all.csv;python ../bin/plot.py --pe -y all.csv`
echo $figs

base=`basename ${PWD}`
CONVERT=$(which convert 2> /dev/null)
if [ ! "x$CONVERT" = "x" ]
then
    $CONVERT -density 300 ${figs} ${base}.pdf
    rm -f ${figs}
else
    echo "convert not installed" >&2
fi
