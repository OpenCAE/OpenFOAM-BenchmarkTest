wmakeDir=wmake/rules/A64FXFcc

for OPT in \
"-O3" \
"-Kfast" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kfp_relaxed -Kfz -Kilfunc -Kmfunc -Komitfp -Ksimd_packed_promotion" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kfp_relaxed -Kfz -Kilfunc -Kmfunc -Komitfp" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kfp_relaxed -Kfz -Kilfunc -Kmfunc" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kfp_relaxed -Kfz -Kilfunc" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kfp_relaxed -Kfz" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kfp_relaxed" \
"-O3 -Keval -Kfast_matmul -Kfp_contract" \
"-O3 -Keval -Kfast_matmul" \
"-O3 -Keval" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kfz -Kilfunc -Kmfunc -Komitfp -Ksimd_packed_promotion" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kfz -Kilfunc -Kmfunc -Komitfp" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kfz -Kilfunc -Kmfunc" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kfz -Kilfunc" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kfz" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kilfunc -Kmfunc -Komitfp -Ksimd_packed_promotion" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kilfunc -Kmfunc -Komitfp" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kilfunc -Kmfunc" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kilfunc" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kmfunc -Komitfp -Ksimd_packed_promotion" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kmfunc -Komitfp" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Kmfunc" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Komitfp -Ksimd_packed_promotion" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Komitfp" \
"-O3 -Keval -Kfast_matmul -Kfp_contract -Ksimd_packed_promotion" \

do
file=$(mktemp)
cat > $file <<EOF
c++DBUG=
c++OPT=$OPT
EOF
hash=$(md5sum $file)
hash=${hash%% *}
echo "hash=$hash"
cp -a $file $wmakeDir/c++$hash
sed s/^c++/c/ $file > $wmakeDir/c$hash
cat > Fcc-$hash.sh <<EOF
#!/bin/bash
#PJM -L rscunit=fx
#PJM -L rscgrp=fx-small
#PJM -L node=1
#PJM -L elapse=48:00:00
#PJM --mpi proc=48
#PJM -j
#PJM -S
module purge
module load tcs/1.2.26
source \$HOME/OpenFOAM/OpenFOAM-v1612+/etc/bashrc WM_COMPILER=Fcc WM_MPLIB=FJMPI WM_COMPILE_OPTION=$hash
export WM_NCOMPPROCS=3
(cd src 
wmakeLnInclude -u OpenFOAM
wmakeLnInclude -u OSspecific/POSIX
OSspecific/POSIX/Allwmake
Pstream/Allwmake
)
wmake src/thermophysicalModels/properties/solidMixtureProperties
wmake src/thermophysicalModels/properties/solidProperties
wmake src/fileFormats
wmake src/thermophysicalModels/thermophysicalFunctions
wmake src/Pstream/mpi
wmake src/surfMesh
wmake src/thermophysicalModels/properties/liquidProperties
wmake src/OpenFOAM
wmake src/triSurface
wmake src/thermophysicalModels/properties/liquidMixtureProperties
wmake src/thermophysicalModels/specie
wmake src/meshTools
wmake src/thermophysicalModels/solidSpecie
wmake src/finiteVolume
wmake src/lagrangian/basic
wmake src/mesh/extrudeModel
wmake src/TurbulenceModels/turbulenceModels
wmake src/transportModels/twoPhaseMixture
wmake src/conversion
wmake src/transportModels/compressible
wmake src/dynamicMesh
wmake src/transportModels/incompressible
wmake src/thermophysicalModels/basic
wmake src/sampling
wmake src/parallel/decompose/decompositionMethods
wmake src/TurbulenceModels/incompressible
wmake src/thermophysicalModels/reactionThermo
wmake src/thermophysicalModels/SLGThermo
wmake src/parallel/distributed
wmake src/thermophysicalModels/solidThermo
wmake src/thermophysicalModels/radiation
wmake src/TurbulenceModels/compressible
wmake src/fvOptions
wmake applications/solvers/incompressible/pimpleFoam
wmake src/lagrangian/distributionModels
wmake src/ODE
wmake src/fvAgglomerationMethods/pairPatchAgglomeration
wmake src/thermophysicalModels/chemistryModel
wmake src/functionObjects/field
EOF
echo "##$wmakeDir/c++$hash" >> Fcc-$hash.sh
sed s/^/#/ $file >> Fcc-$hash.sh

pjsub Fcc-$hash.sh
done
