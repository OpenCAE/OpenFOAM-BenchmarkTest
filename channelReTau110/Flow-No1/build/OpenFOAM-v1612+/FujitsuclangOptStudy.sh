wmakeDir=wmake/rules/linuxARM64Fujitsuclang

for OPT in \
"-O3 -march=armv8.2-a+nosve+fp16" \
"-Ofast -march=armv8.2-a+nosve+fp16" \

do
file=$(mktemp)
cat > $file <<EOF
c++DBUG=
c++OPT=$OPT
EOF
hash=$(md5sum $file)
hash=${hash%% *}
echo "hash=$hash."
cp -a $file $wmakeDir/c++$hash
sed s/^c++/c/ $file > $wmakeDir/c$hash
cat > Fujitsuclang-$hash.sh <<EOF
#!/bin/bash
#PJM -L rscunit=fx
#PJM -L rscgrp=fx-small
#PJM -L node=1
#PJM -L elapse=12:00:00
#PJM --mpi proc=48
#PJM -j
#PJM -S
module purge
module load tcs/1.2.26
source \$HOME/OpenFOAM/OpenFOAM-v1612+/etc/bashrc WM_COMPILER=Fujitsuclang WM_MPLIB=FJMPI WM_COMPILE_OPTION=$hash
export WM_NCOMPPROCS=48
#./Allwmake -k
wmake src/thermophysicalModels/properties/solidMixtureProperties
wmake src/thermophysicalModels/properties/solidProperties
wmake src/fileFormats
wmake src/thermophysicalModels/thermophysicalFunctions
wmake src/Pstream/mpi
wmake src/surfMesh
wmake src/thermophysicalModels/properties/liquidProperties
wmake src/OpenFOAM
wmake src/triSurface
wmake src/thermophysicalModels/properties/liquidMixtureProperties
wmake src/thermophysicalModels/specie
wmake src/meshTools
wmake src/thermophysicalModels/solidSpecie
wmake src/finiteVolume
wmake src/lagrangian/basic
wmake src/mesh/extrudeModel
wmake src/TurbulenceModels/turbulenceModels
wmake src/transportModels/twoPhaseMixture
wmake src/conversion
wmake src/transportModels/compressible
wmake src/dynamicMesh
wmake src/transportModels/incompressible
wmake src/thermophysicalModels/basic
wmake src/sampling
wmake src/parallel/decompose/decompositionMethods
wmake src/TurbulenceModels/incompressible
wmake src/thermophysicalModels/reactionThermo
wmake src/thermophysicalModels/SLGThermo
wmake src/parallel/distributed
wmake src/thermophysicalModels/solidThermo
wmake src/thermophysicalModels/radiation
wmake src/TurbulenceModels/compressible
wmake src/fvOptions
wmake applications/solvers/incompressible/pimpleFoam
wmake src/lagrangian/distributionModels
wmake src/ODE
wmake src/fvAgglomerationMethods/pairPatchAgglomeration
wmake src/thermophysicalModels/chemistryModel
wmake src/functionObjects/field
EOF
echo "##$wmakeDir/c++$hash" >> Fujitsuclang-$hash.sh
sed s/^/#/ $file >> Fujitsuclang-$hash.sh

pjsub Fujitsuclang-$hash.sh
done

