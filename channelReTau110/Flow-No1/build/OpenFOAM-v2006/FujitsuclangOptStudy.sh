wmakeDir=wmake/rules/linuxARM64Fujitsuclang

for OPT in \
"-Ofast -march=armv8.2-a+nosve+fp16" \
"-O3 -march=armv8.2-a+nosve+fp16" \
"-O3 -ffj-fast-matmul -march=armv8.2-a+nosve+fp16" \
"-O3 -ffast-math -march=armv8.2-a+nosve+fp16 " \
"-O3 -ffp-contract=fast -march=armv8.2-a+nosve+fp16" \
"-O3 -ffj-fp-relaxed -march=armv8.2-a+nosve+fp16" \
"-O3 -ffj-ilfunc -march=armv8.2-a+nosve+fp16" \
"-O3 -fbuiltin -march=armv8.2-a+nosve+fp16" \
"-O3 -fomit-frame-pointer -march=armv8.2-a+nosve+fp16" \
"-O3 -finline-functions -march=armv8.2-a+nosve+fp16" \

do
file=$(mktemp)
cat > $file <<EOF
c++DBUG=
c++OPT=$OPT
EOF
hash=$(md5sum $file)
hash=${hash%% *}
echo "hash=$hash."
cp -a $file $wmakeDir/c++$hash
sed s/^c++/c/ $file > $wmakeDir/c$hash
cat > Fujitsuclang-$hash.sh <<EOF
#!/bin/bash
#PJM -L rscunit=fx
#PJM -L rscgrp=fx-small
#PJM -L node=1
#PJM -L elapse=12:00:00
#PJM --mpi proc=48
#PJM -j
#PJM -S
module purge
module load tcs/1.2.26
source \$HOME/OpenFOAM/OpenFOAM-v2006/etc/bashrc WM_COMPILER=Fujitsuclang WM_MPLIB=USERMPI WM_COMPILE_OPTION=$hash
export WM_NCOMPPROCS=48
wmake/src/Allmake
src/Pstream/Allwmake
src/OSspecific/POSIX/Allwmake
wmake src/thermophysicalModels/specie
wmake src/OpenFOAM
wmake src/thermophysicalModels/thermophysicalProperties
wmake src/thermophysicalModels/solidSpecie
wmake src/fileFormats
wmake src/surfMesh
wmake src/meshTools
wmake src/finiteVolume
wmake src/mesh/extrudeModel
wmake src/lagrangian/basic
wmake src/transportModels/compressible
wmake src/TurbulenceModels/turbulenceModels
wmake src/transportModels/twoPhaseMixture
wmake src/dynamicMesh
wmake src/thermophysicalModels/basic
wmake src/transportModels/incompressible
wmake src/sampling
wmake src/parallel/decompose/decompositionMethods
wmake src/dynamicFvMesh
wmake src/thermophysicalModels/reactionThermo
wmake src/TurbulenceModels/incompressible
wmake src/parallel/distributed
wmake src/topoChangerFvMesh
wmake src/thermophysicalModels/solidThermo
wmake src/thermophysicalModels/SLGThermo
wmake src/thermophysicalModels/radiation
wmake src/TurbulenceModels/compressible
wmake src/fvOptions
wmake src/atmosphericModels
wmake applications/solvers/incompressible/pimpleFoam
wmake src/ODE
wmake src/lagrangian/distributionModels
wmake src/fvAgglomerationMethods/pairPatchAgglomeration
wmake src/thermophysicalModels/chemistryModel
wmake src/functionObjects/field
exit
EOF
echo "##$wmakeDir/c++$hash" >> Fujitsuclang-$hash.sh
sed s/^/#/ $file >> Fujitsuclang-$hash.sh

pjsub Fujitsuclang-$hash.sh
done
