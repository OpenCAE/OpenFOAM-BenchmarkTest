# Flow-No1

## General information

* Measurer: IMANO Masashi
* Date: Aug 2020

## Hardware

* Name: Flow (http://www.icts.nagoya-u.ac.jp/en/sc/)

## OpenFOAM patch and build script for Fujitsu compiler

https://gitlab.com/OpenCAE/OpenFOAM-BenchmarkTest/-/tree/master/channelReTau110/Flow-No1/build
