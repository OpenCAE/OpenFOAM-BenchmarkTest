#!/bin/bash

function generate_fvSolution () {
    if [  "$fvSolution_solvers_p_solver" = "PCG" -o "$fvSolution_solvers_p_solver" = "PPCG" -o "$fvSolution_solvers_p_solver" = "PPCR" ]
    then
	if [ "$fvSolution_solvers_p_preconditioner" = "GAMG" ]
	then
	    fvSolution_solvers_p_preconditioner_smoother=`cat << EOM
        preconditioner {
            preconditioner  $fvSolution_solvers_p_preconditioner;
            smoother $fvSolution_solvers_p_smoother;
            agglomerator $fvSolution_solvers_p_agglomerator;
            cacheAgglomeration $fvSolution_solvers_p_cacheAgglomeration;
            interpolateCorrection $fvSolution_solvers_p_interpolateCorrection;
            maxPostSweeps $fvSolution_solvers_p_maxPostSweeps;
            maxPreSweeps $fvSolution_solvers_p_maxPreSweeps;
            mergeLevels $fvSolution_solvers_p_mergeLevels;
            nCellsInCoarsestLevel $fvSolution_solvers_p_nCellsInCoarsestLevel;
            nFinestSweeps $fvSolution_solvers_p_nFinestSweeps;
            nPostSweeps $fvSolution_solvers_p_nPostSweeps;
            nPreSweeps $fvSolution_solvers_p_nPreSweeps;
            postSweepsLevelMultiplier $fvSolution_solvers_p_postSweepsLevelMultiplier;
            preSweepsLevelMultiplier $fvSolution_solvers_p_preSweepsLevelMultiplier;
            directSolveCoarsest $fvSolution_solvers_p_directSolveCoarsest;
            nVcycles $fvSolution_solvers_p_nVcycles;
        }
EOM
`
	else
	    fvSolution_solvers_p_preconditioner_smoother="preconditioner $fvSolution_solvers_p_preconditioner;"
	fi
    elif [ "$fvSolution_solvers_p_solver" = "GAMG" ]
    then
	fvSolution_solvers_p_preconditioner_smoother=`cat << EOM
        smoother $fvSolution_solvers_p_smoother;
        agglomerator $fvSolution_solvers_p_agglomerator;
        cacheAgglomeration $fvSolution_solvers_p_cacheAgglomeration;
        interpolateCorrection $fvSolution_solvers_p_interpolateCorrection;
        maxPostSweeps $fvSolution_solvers_p_maxPostSweeps;
        maxPreSweeps $fvSolution_solvers_p_maxPreSweeps;
        mergeLevels $fvSolution_solvers_p_mergeLevels;
        nCellsInCoarsestLevel $fvSolution_solvers_p_nCellsInCoarsestLevel;
        nFinestSweeps $fvSolution_solvers_p_nFinestSweeps;
        nPostSweeps $fvSolution_solvers_p_nPostSweeps;
        nPreSweeps $fvSolution_solvers_p_nPreSweeps;
        postSweepsLevelMultiplier $fvSolution_solvers_p_postSweepsLevelMultiplier;
        preSweepsLevelMultiplier $fvSolution_solvers_p_preSweepsLevelMultiplier;
        directSolveCoarsest $fvSolution_solvers_p_directSolveCoarsest;
        coarsestLevelCorr
        {
            preconditioner DIC;
            solver $fvSolution_solvers_p_coarsestLevelCorr_solver;
            relTol $fvSolution_solvers_p_coarsestLevelCorr_relTol;
        }
EOM
`
    fi
    if [ "$fvSolution_solvers_pFinal_solver" = "PCG" -o "$fvSolution_solvers_pFinal_solver" = "PPCG" -o "$fvSolution_solvers_pFinal_solver" = "PPCR" ]
    then
	if [ "$fvSolution_solvers_pFinal_preconditioner" = "GAMG" ]
	then
	    fvSolution_solvers_pFinal_preconditioner_smoother=`cat << EOM
        preconditioner {
            preconditioner  $fvSolution_solvers_pFinal_preconditioner;
            smoother $fvSolution_solvers_pFinal_smoother;
            agglomerator $fvSolution_solvers_pFinal_agglomerator;
            cacheAgglomeration $fvSolution_solvers_pFinal_cacheAgglomeration;
            interpolateCorrection $fvSolution_solvers_pFinal_interpolateCorrection;
            maxPostSweeps $fvSolution_solvers_pFinal_maxPostSweeps;
            maxPreSweeps $fvSolution_solvers_pFinal_maxPreSweeps;
            mergeLevels $fvSolution_solvers_pFinal_mergeLevels;
            nCellsInCoarsestLevel $fvSolution_solvers_pFinal_nCellsInCoarsestLevel;
            nFinestSweeps $fvSolution_solvers_pFinal_nFinestSweeps;
            nPostSweeps $fvSolution_solvers_pFinal_nPostSweeps;
            nPreSweeps $fvSolution_solvers_pFinal_nPreSweeps;
            postSweepsLevelMultiplier $fvSolution_solvers_pFinal_postSweepsLevelMultiplier;
            preSweepsLevelMultiplier $fvSolution_solvers_pFinal_preSweepsLevelMultiplier;
            directSolveCoarsest $fvSolution_solvers_pFinal_directSolveCoarsest;
            nVcycles $fvSolution_solvers_pFinal_nVcycles;
        }
EOM
`
	else
	    fvSolution_solvers_pFinal_preconditioner_smoother="preconditioner $fvSolution_solvers_pFinal_preconditioner;"
	fi
    elif [ "$fvSolution_solvers_pFinal_solver" = "GAMG" ]
    then
	fvSolution_solvers_pFinal_preconditioner_smoother=`cat << EOM
        smoother $fvSolution_solvers_pFinal_smoother;
        agglomerator $fvSolution_solvers_pFinal_agglomerator;
        cacheAgglomeration $fvSolution_solvers_pFinal_cacheAgglomeration;
        interpolateCorrection $fvSolution_solvers_pFinal_interpolateCorrection;
        maxPostSweeps $fvSolution_solvers_pFinal_maxPostSweeps;
        maxPreSweeps $fvSolution_solvers_pFinal_maxPreSweeps;
        mergeLevels $fvSolution_solvers_pFinal_mergeLevels;
        nCellsInCoarsestLevel $fvSolution_solvers_pFinal_nCellsInCoarsestLevel;
        nFinestSweeps $fvSolution_solvers_pFinal_nFinestSweeps;
        nPostSweeps $fvSolution_solvers_pFinal_nPostSweeps;
        nPreSweeps $fvSolution_solvers_pFinal_nPreSweeps;
        postSweepsLevelMultiplier $fvSolution_solvers_pFinal_postSweepsLevelMultiplier;
        preSweepsLevelMultiplier $fvSolution_solvers_pFinal_preSweepsLevelMultiplier;
        directSolveCoarsest $fvSolution_solvers_pFinal_directSolveCoarsest;
        coarsestLevelCorr
        {
            preconditioner DIC;
            solver $fvSolution_solvers_pFinal_coarsestLevelCorr_solver;
            relTol $fvSolution_solvers_pFinal_coarsestLevelCorr_relTol;
        }
EOM
`
    fi
    if [ "$fvSolution_solvers_UFinal_solver" = "smoothSolver" ]
    then
	fvSolution_solvers_UFinal_preconditioner_smoother="smoother $fvSolution_solvers_UFinal_smoother;"
    elif [ "$fvSolution_solvers_UFinal_solver" = "PBiCG" -o "$fvSolution_solvers_UFinal_solver" = "PBiCGStab" ]
    then
	fvSolution_solvers_UFinal_preconditioner_smoother="preconditioner $fvSolution_solvers_UFinal_preconditioner;"
    fi
    rm -f include/fvSolution
    cat > include/fvSolution <<EOF
solvers
{
  p
  {
    maxIter 5000;
    tolerance 1e-6;
    relTol $fvSolution_solvers_p_relTol;
    solver $fvSolution_solvers_p_solver;
$fvSolution_solvers_p_preconditioner_smoother
  }
  pFinal
  {
    maxIter 5000;
    tolerance 1e-6;
    relTol 0;
    solver $fvSolution_solvers_pFinal_solver;
$fvSolution_solvers_pFinal_preconditioner_smoother
  }
  UFinal
  {
    maxIter 5000;
    tolerance 1e-05;
    relTol 0;
    solver $fvSolution_solvers_UFinal_solver;
$fvSolution_solvers_UFinal_preconditioner_smoother
  }
}
PIMPLE
{
  nOuterCorrectors 1;
  nCorrectors 2;
  nNonOrthogonalCorrectors 0;
  pRefCell 0;
  pRefValue 0;
}
EOF
}

#
# Main
#
# 最適化スクリプトで生成されたdecomposeParDict.sh向けのパラメータ定義ファイルを評価
source ../decomposeParDict.sh.param
# 最適化スクリプトで生成されたパラメータ定義ファイルを評価
source $0.param
# ppn(process per node)とノード数から，領域分割数を算出
domains_node=${node%:*}
domains_node=${domains_node//x/*}
domains_node=$(echo $domains_node | bc)
numberOfSubdomains=$(echo "${ppn}*${domains_node}" | bc)
# 元のケースディレクトリから必要なディレクトリをリンク
if [ ! -d include ]
then
    mkdir include
    (cd include
	ln -s  ../../include/* ./
    )
fi
for file in 0 constant system
do
    [ -L $file ] || ln -s ../$file .
done
# 領域分割したケースディレクトリからプロセッサディレクトリをリンク
if [ $numberOfSubdomains -gt 1 ]
then 
    i=0
    while [ $i -lt $numberOfSubdomains ] 
    do
	file=processor$i
	[ -L $file ] || ln -s ../$file .
	i=`expr $i + 1` 
    done
fi
# Generate fvSolution
generate_fvSolution
# Generage batch and include files
case $OpenFOAM_VERSION in
    RapidCFD-*)
	rm -f include/OptimisationSwitches
	cat > include/OptimisationSwitches <<EOF
OptimisationSwitches
{
  gpuDirectTransfer $gpuDirectTransfer;
  favourSpeedOverMemory $favourSpeedOverMemory;
}
EOF
	case $policy in
	    compact)
		deviceArray=("0" "1" "2" "3" "4" "5" "6" "7")
		;;
	    scatter)
		deviceArray=("0" "4" "1" "5" "2" "6" "3" "7")
		;;
	esac
	devices="'("
	i=0
	startdevice=0
	deviceI=$startdevice
	while [ "$i" -lt "$numberOfSubdomains" ]
	do
	    devices="$devices ${deviceArray[$deviceI]}"
	    i=$(expr $i + 1)
	    deviceI=$(expr $deviceI + 1)
	    if [ "$deviceI" -eq $ppn ];then
		deviceI=$startdevice
	    fi
	done
	devices="$devices )'"
	;;
esac

project=gz00
if [ "$system" = "share" ]
then
    #    rscgrp=share
    rscgrp=tutorial-share
    project=gt00
elif  [ "$system" = "Odyssey" ]
then
    rscgrp=regular-o
else
    rscgrp=regular-a
fi

(
    cat <<EOF
#!/bin/sh
#PJM -g $project
#PJM -L rscgrp=$rscgrp
#PJM -L elapse=0:10:00
#PJM -S
#PJM -j
EOF
    if [ $numberOfSubdomains -gt 1 ]
    then
    cat <<EOF
#PJM --mpi proc=$numberOfSubdomains
EOF
    fi

    if [ "$system" = "share" ]
    then
	cat <<EOF
#PJM -L gpu=$ppn
EOF
    else
	cat <<EOF
#PJM -L node=$node
EOF
    fi

    if [ "$system" = "Odyssey" ]
    then
	cat <<EOF
module purge
module load fj/$fj_version
module load fjmpi/$fj_version
EOF
    else
	cat <<EOF
module purge
module load cuda/11.2
module load gcc/8.3.1
module load ompi-cuda/4.1.1-11.2
EOF
    fi

    case $OpenFOAM_VERSION in
	openfoam/*)
	    cat <<EOF
module load $OpenFOAM_VERSION
source \$WM_PROJECT_DIR/etc/bashrc \\
EOF
	    ;;
	OpenFOAM-*)
	    cat <<EOF
source /work/gt00/z30106/OpenFOAM/$OpenFOAM_VERSION/etc/bashrc \\
EOF
	    ;;
	RapidCFD-*)
	    cat <<EOF
source /work/gt00/z30106/RapidCFD/$OpenFOAM_VERSION/etc/bashrc \\
EOF
	    ;;
    esac
    
    cat <<EOF
WM_COMPILER_TYPE=$WM_COMPILER_TYPE \
WM_COMPILER=$WM_COMPILER \
WM_PRECISION_OPTION=$WM_PRECISION_OPTION \
WM_LABEL_SIZE=$WM_LABEL_SIZE \
WM_COMPILE_OPTION=$WM_COMPILE_OPTION \
WM_MPLIB=$WM_MPLIB
EOF

    if [ "$profiling" = "fipp" ]
    then
	cat <<EOF
fipp -C -d./fipp.\$PJM_JOBID \\
EOF
    fi

    if [ $numberOfSubdomains -eq 1 ]
    then
	cat <<EOF
pimpleFoam >& log.$0.\$PJM_JOBID 
EOF
    else
	if [ "$system" = "Odyssey" ]
	then
	    cat <<EOF
mpiexec -of log.$0.\$PJM_JOBID -n \$PJM_MPI_PROC pimpleFoam -parallel
EOF
	else
	    cat <<EOF
mpiexec -display-map -display-devel-map -n \$PJM_MPI_PROC -machinefile \$PJM_O_NODEINF -npernode $ppn pimpleFoam  -devices $devices -parallel >& log.$0.\$PJM_JOBID 
EOF
	fi
    fi
    ) > run.sh

# Submit job
if [ "$system" = "Odyssey" ]
then
    MAX_NUMBER_OF_QUEUE=128
else
    if [ "$system" = "share" ]
    then
	MAX_NUMBER_OF_QUEUE=140
    else
	MAX_NUMBER_OF_QUEUE=4
    fi
fi
while :
do
    sleep $(($RANDOM % 5))
    if [ "$system" = "Odyssey" ]
    then
	nq=$(pjstat | grep "^[0-9].* .*-o " | wc -l)
    else
	nq=$(pjstat | grep -e "^[0-9].* .*-a "  -e "^[0-9].* share.* " | wc -l)
    fi
    [ "$nq" -lt "$MAX_NUMBER_OF_QUEUE" ] &&  break
done
JOBID=$(pjsub -z jid run.sh)
echo $JOBID
# Waiting for job
log=log.$0.$JOBID
state=run.sh.$JOBID.stats
while :
do
    if [ -f $log ]
    then
	if grep 'No Iterations 500[01]' $log >& /dev/null -o
	    grep 'ERROR' $log >& /dev/null
	then
	    pjdel $JOBID >& /dev/null
	    echo "Killed" >> $log
	    exit 1
	elif grep '^End' $log >& /dev/null
	then
	    exit 0
	fi
	sleep 3
    else
	sleep 3
    fi
done
exit 0
