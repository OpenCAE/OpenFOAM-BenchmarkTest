#!/bin/bash
#
MAX_NUMBER_OF_QUEUE=128
# 最適化スクリプトで生成されたパラメータ定義ファイルを評価
source $0.param
# ppn(process per node)とノード数から，領域分割数を算出
domains_node=${node%:*}
domains_node=${domains_node//x/*}
domains_node=$(echo $domains_node | bc)
numberOfSubdomains=$(echo "${ppn}*${domains_node}" | bc)
# 元のケースディレクトリから必要なディレクトリをリンク
if [ ! -d include ]
then
    mkdir include
    (cd include
	ln -s  ../../include/* ./
    )
fi
for file in 0 constant system
do
    [ -L $file ] || ln -s ../$file .
done
if [ $numberOfSubdomains -eq 1 ]
then
    JOBID=00000
    echo "End" > log.decomposeParDict.sh.$JOBID
    echo $JOBID
    exit 0
fi
# decomposeParDictを生成
rm -f include/decomposeParDict
(
    cat <<EOF
numberOfSubdomains $numberOfSubdomains;
method $method;
preservePatches (sides_half0 sides_half1 inout_half0 inout_half1);
EOF
    if [ "$method" = "multiLevel" ]
    then
	cat <<EOF
multiLevelCoeffs
{
  method  scotch;
  domains ($ppn $domains_node);
}
EOF
    fi
) > include/decomposeParDict
echo "" > include/OptimisationSwitches
batch=decomposePar.sh
cat > $batch <<EOF
#!/bin/sh
module purge
module load gcc/8.3.1 impi/2021.2.0 openfoam/v2012
source \${WM_PROJECT_DIR}/etc/bashrc
decomposePar >& log.$0.\$PJM_JOBID
EOF
own=${PWD##*/}
unset same
for other in $(cd ..;ls -d _* | grep -v $own)
do
    if diff -q ../$other/include/decomposeParDict include/decomposeParDict >& /dev/null
    then
	same=$other
	break
    fi
done
if [ -v same ]
then
    [ ! -L processor0 -a ! -L processors$numberOfSubdomains ] && ln -s ../$same/processor* ./
    JOBID=00000
    echo "End" > log.decomposeParDict.sh.$JOBID
    echo $JOBID
    exit 0
fi
# Submit job
while :
do
    nq=$(pjstat | grep "^[0-9]* .* prepost " | wc -l)
    [ "$nq" -eq 0 ] &&  break
    sleep 5
done
log=$(mktemp)
cat $batch| pjsub --interact -g gz00 -L rscgrp=prepost -L node=1 -z jid &> $log 
JOBID=$(head -n 1 $log)
mv $log $batch.$JOBID.out
echo $JOBID
exit 0
