# ITO-mesh_any-No8

## Hardware

* Name: ITO Subsystem A (https://www.cc.kyushu-u.ac.jp/scp/system/ITO/)

## Solve batch script

Solve batch script name(legend of plot):

nPPN-FOAM_VERSION-COMPILER_TYPE-COMPILER-COMPILE_OPTION-PRECISION_OPTION-MPLIB-OPTIONS

### PPN

Number of processes per node.

* 36

### FOAM_VERSION

OpenFOAM version.

* v1806: OpenFOAM-v1806

### COMPILER_TYPE

* system: System compiler

### COMPILER

* Gcc4_8_5: Gcc 4.8.5

### COMPILE_OPTION

* Opt: -O3 (default)

### PRECISION_OPTION

* DP
* SPDP

### MPLIB

* INTELMPI2018_3_222: Intel MPI 2018.3.222

### OPTIONS

#### mpirun options for Intel MPI

* Default settings:

```
unset I_MPI_PIN_DOMAIN
export I_MPI_PERHOST=${PPN}
export I_MPI_HYDRA_BOOTSTRAP=rsh
export I_MPI_HYDRA_BOOTSTRAP_EXEC=/bin/pjrsh
export I_MPI_HYDRA_HOST_FILE="${PJM_O_NODEINF}"
```

* Additional settings:
  * 6: export I_MPI_DEBUG=6
  * F: export I_MPI_FALLBACK=0

# How to plot results

Link settings and shell scripts if Allrun.plot does not exists.

```bash
../bin/linkSettings.sh ITO-mesh_any-No8
```

Execute Allrun.plot

```bash
./Allrun.plot
```

## Note
