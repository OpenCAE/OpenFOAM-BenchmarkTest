#!/bin/sh

maxNumberOfSampling=5

figs=`\
python ../bin/plot.py all.csv \
-L 1 \
-m $maxNumberOfSampling \
--sph \
--ExecutionTimePerStep ExecutionTimePerStep \
--tickFontSize 8;
python ../bin/plot.py all.csv \
-L 1 \
-m $maxNumberOfSampling \
--pe -y \
--ExecutionTimePerStep ExecutionTimePerStep \
--tickFontSize 8`

base=`basename ${PWD}`
CONVERT=$(which convert 2> /dev/null)
if [ "x$CONVERT" != "x" ]
then
    $CONVERT -density 300 ${figs} ${base}.pdf
    rm -f ${figs}
else
    echo "convert not installed" >&2
fi
