WORKDIR=${PWD##*/}

case ${WORKDIR} in
    *-mesh_0.37M*)
	WALLTIME="00:05:00"
	MAX_NUMBER_OF_LOOP=1
	;;
    *-mesh_3M*)
	WALLTIME="00:05:00"
	MAX_NUMBER_OF_LOOP=5
	;;
    *-mesh_24M*)
	WALLTIME="00:10:00"
	MAX_NUMBER_OF_LOOP=5
	;;
esac

case "$configuration" in
    n32)
	case ${WORKDIR} in
	    *-mesh_0.37M*)
		decomposeParDictArray=(
mpi_00032-method_scotch
mpi_00064-method_scotch
mpi_00128-method_scotch
mpi_00256-method_scotch
		)
		;;
	    *-mesh_3M*)
		decomposeParDictArray=(
mpi_00032-method_scotch
mpi_00064-method_scotch
mpi_00128-method_scotch
mpi_00256-method_scotch
		)
		;;
	    *-mesh_24M*)
		decomposeParDictArray=(
mpi_00032-method_scotch
mpi_00064-method_scotch
mpi_00128-method_scotch
mpi_00256-method_scotch
		)
		;;
	esac
	solveBatchArray=(
	    n32-v1906-system-Gcc4_8_5-Opt-DP-INTELMPI2018_3_222-6F
	    n32-v1906-system-Gcc4_8_5-Opt-SPDP-INTELMPI2018_3_222-6F
	)
   ;;
    n36)
	case ${WORKDIR} in
	    *-mesh_0.37M*)
		decomposeParDictArray=(
mpi_00036-method_scotch
mpi_00072-method_scotch
mpi_00144-method_scotch
mpi_00288-method_scotch
	)
		;;
	    *-mesh_3M*)
		decomposeParDictArray=(
mpi_00036-method_scotch
mpi_00072-method_scotch
mpi_00144-method_scotch
mpi_00288-method_scotch
	)
		;;
	    *-mesh_24M*)
		decomposeParDictArray=(
mpi_00036-method_scotch
mpi_00072-method_scotch
mpi_00144-method_scotch
mpi_00288-method_scotch
		)
		;;
	esac
	solveBatchArray=(
	    n36-v1906-system-Gcc4_8_5-Opt-DP-INTELMPI2018_3_222-6F
	    n36-v1906-system-Gcc4_8_5-Opt-SPDP-INTELMPI2018_3_222-6F
	)
   ;;
esac

fvSolutionArray=(
PCG-DIC
)

BATCH_PRE=0
BATCH_DECOMPOSEPAR=0
BATCH_SOLVE=1

MAX_NUMBER_OF_QUEUE=128

NumberOfBatchQueue()
{
    pjstat | grep "^[0-9]" | wc -l
}

BatchSubmit()
{
    local BATCHFILE=$1
    local MPI=$2

    echo "BATCHFILE: $BATCHFILE" 
    echo "MPI: $MPI" 

    if [ "$BATCHFILE" = "pre.sh" -o "$BATCHFILE" = "decomposePar.sh" ];then
        local NPROCS_PER_NODE=1
        local NODE=1
    else
        local NPROCS_PER_NODE=`echo $BATCHFILE | cut  -d "-"  -f 1 | tr -d 'n'`
        local NODE=`echo "($MPI + $NPROCS_PER_NODE - 1)/ $NPROCS_PER_NODE" | bc`
    fi

    echo "NPROCS_PER_NODE: $NPROCS_PER_NODE" 
    echo "NODE: $NODE" 

    if [ "$BATCHFILE" != "pre.sh" -a "$BATCHFILE" != "decomposePar.sh" ];then
	local MPI_TMP=`echo "$NODE * $NPROCS_PER_NODE" | bc`
	echo "NODE * NPROCS_PER_NODE: $MPI_TMP"
        if [ $MPI -ne $MPI_TMP ];then
	    echo "Number of MPI is mismatch. Skip running."
  	    return 0
	fi  
    fi

    if [ $NODE -eq 1 ]
    then
      rscgrp=ito-ss-dbg
    elif [ $NODE -le 4 ]
    then
      rscgrp=ito-s-dbg
    elif [ $NODE -le 16 ]
    then
      rscgrp=ito-m-dbg
    elif [ $NODE -le 64 ]
    then
      rscgrp=ito-l
    elif [ $NODE -le 128 ]
    then
      rscgrp=ito-xl
    else
      rscgrp=ito-xxl
    fi

    command="pjsub -j -S -L rscgrp=$rscgrp,rscunit=ito-a,vnode=${NODE},vnode-core=36,elapse=${WALLTIME} --mpi proc=${MPI} ${BATCHFILE}"
    echo $command
    $command
}
