# Oracle_Cloud_BM.HPC2.36-mesh_24M-No3

## General information

* Measurer:  Masashi Imano
* Date: Nov 2018

## Benchmark condition

* Number of cells：23961600(24M)
* Number of nodes: 1-26
* Number of processors per node: 32, 36
* Number of trial: 1

## Other information

See https://gitlab.com/OpenCAE/OpenFOAM-BenchmarkTest/blob/master/channelReTau110/Oracle_Cloud_BM.HPC2.36-mesh_any-No3/README.md
