#!/bin/bash
#
MAX_NUMBER_OF_QUEUE=128
# 最適化スクリプトで生成されたパラメータ定義ファイルを評価
source $0.param
# ppn(process per node)とノード数から，領域分割数を算出
numberOfSubdomains=`expr $decomposeParDict_ppn \* $decomposeParDict_node`
# 元のケースディレクトリから必要なディレクトリをリンク
if [ ! -d include ]
then
    mkdir include
    (cd include
	ln -s  ../../include/* ./
    )
fi
for file in 0 constant system
do
    [ -L $file ] || ln -s ../$file .
done
# decomposeParDictを生成
rm -f include/decomposeParDict
cat > include/decomposeParDict <<EOF
numberOfSubdomains $numberOfSubdomains; // 領域分割数(並列数)
method $decomposeParDict_method; // 分割手法
preservePatches (sides_half0 sides_half1 inout_half0 inout_half1);
multiLevelCoeffs
{
  method  scotch;
  domains ($decomposeParDict_ppn $decomposeParDict_node);
}
EOF
gid=$(id -gn)
uid=$(id -un)
cat > decomposePar.sh <<EOF
#!/bin/bash
#PJM -g $gid
#PJM -L rscgrp=short
#PJM -L node=1
#PJM -L elapse=4:00:00
#PJM --mpi proc=1
#PJM -S
#PJM -j
module purge
module load intel/2020.1.217
source /work/$gid/$uid/OpenFOAM/OpenFOAM-v2006/etc/bashrc \
WM_COMPILER_TYPE=ThirdParty \
WM_COMPILER=Gcc10_2_0 \
WM_MPLIB=INTELMPI2019_7_217
decomposePar >& log.$0.\$PJM_JOBID
EOF
while :
do
    nq=`pjstat | grep "^[0-9]" | wc -l`
    [ "$nq" -lt "$MAX_NUMBER_OF_QUEUE" ] &&  break
    sleep 5
done
MSG=`pjsub decomposePar.sh`
JOBID=`echo $MSG | awk '{if ($7=="submitted.") {print $6} else {print "-1"} }'`
echo $JOBID
exit 0
